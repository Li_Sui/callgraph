#author amjed tahir (a.tahir@massey.ac.nz)
#make sure that vioplot package and depedencies are installed using the following command.


# must install the following packages
library(vioplot)
library(rstudioapi)

# get current working directory where r script is located in the repo
current_working_dir <- dirname(rstudioapi::getActiveDocumentContext()$path)
path <- setwd(current_working_dir)


recall_builtin_generated <- read.csv(file = file.path(path,"spreadsheets","recall-builtin-vs-generated.csv"))
recall_base_reflection <- read.csv(file = file.path(path,"spreadsheets","recall-base-vs-reflection.csv"))
recall_base_contextsensitive <- read.csv(file = file.path(path,"spreadsheets","recall-base-vs-contextsensitive.csv"))

categorised_FNS_base <- read.csv(file = file.path(path,"spreadsheets","RQ4-boxplot-base.csv"))
categorised_FNS_reflection<- read.csv(file = file.path(path,"spreadsheets","RQ4-boxplot-reflection.csv"))

#FNratio_uncategorised <- read.csv(file = file.path(path,"spreadsheets","uncategorised.csv"))

coverage <- read.csv(file = file.path(path,"spreadsheets","branchcoverage.csv"))

#Recall of the base static analysis with respect to oracles for both lib and superjar mode
png(filename=file.path(path,"graphs","recall-builtin-vs-generated.png"), width = 700, height = 400)
vioplot(recall_builtin_generated$builtin.lib,recall_builtin_generated$builtin.super,recall_builtin_generated$generated.lib,recall_builtin_generated$generated.super,recall_builtin_generated$combined.lib,recall_builtin_generated$combined.super,
        names = c("builtin lib", "builtin super","generated lib", "generated super","combined lib", "combined super"),
        ylab = "recall", col = "gray")
dev.off()


# png(filename=file.path(path,"graphs","recall-base-vs-reflection_seperateTests.png"), width = 650, height = 400)
# vioplot(recall_base_reflection$builtin.base,recall_base_reflection$builtin.ref,recall_base_reflection$Builtin.refLite,
#         recall_base_reflection$generated.base,recall_base_reflection$generated.ref,recall_base_reflection$generated.refLite,
#         recall_base_reflection$combined.base,recall_base_reflection$combined.ref,recall_base_reflection$combined.refLite,
#         names = c("builtin base", "builtin ref","builtin refLite", "generated base","generated ref","generated refLite", "combined base", "combined ref", "combined refLite"), col = "gray")
# dev.off()

#png(filename=file.path(path,"graphs","recall-base-vs-reflection.png"), width = 650, height = 400)
#vioplot(recall_base_reflection$combined.base,recall_base_reflection$combined.ref,
#        names = c("combined base", "combined ref"), col = "gray")
#dev.off()

# png(filename=file.path(path,"graphs","recall-base-vs-contextsensitive_seperateTests.png"), width = 750, height = 400)
# vioplot(recall_base_contextsensitive$builtin.base,recall_base_contextsensitive$builtin.contextsensitive,
#         recall_base_contextsensitive$generated.base,recall_base_contextsensitive$generated.contextsensitive,
#         recall_base_contextsensitive$combined.base,recall_builtin_generated$combined.super,
#         names = c("builtin base", "builtin contextsensitive", "generated base", "generated contextsensitive", "combined base", "combined contextsensitive"), col = "gray")
# dev.off()

#png(filename=file.path(path,"graphs","recall-base-vs-contextsensitive.png"), width = 750, height = 400)
#vioplot(recall_base_contextsensitive$combined.base,recall_base_contextsensitive$combined.contextsensitive,
#        names = c("combined base", "combined contextsensitive"), col = "gray")
#dev.off()

# base vs context-sensitive analysis and reflection analysis
png(filename=file.path(path,"graphs","recall-base-vs-reflection_combined.png"), width = 650, height = 400)
vioplot(recall_base_contextsensitive$combined.base,recall_base_contextsensitive$combined.contextsensitive,recall_base_reflection$combined.base,recall_base_reflection$combined.ref,
        names = c("base (31)", "contextsensitive (31)", "base (20)","ref (20)"), ylab = "recall",col = "gray")
dev.off()

#false negatives in the base static analysis
png(filename=file.path(path,"graphs","categorised_FNS_base.png"), width = 750, height = 400)
vioplot(categorised_FNS_base$DI,categorised_FNS_base$DALL,categorised_FNS_base$DACC,categorised_FNS_base$SYS,categorised_FNS_base$other,
        names = c("DI", "DALL","DACC","SYS", "Other"), ylab = "percentage", xlab = "category", col = "gray")
dev.off()

#false negatives in the static analysis with reflection support
png(filename=file.path(path,"graphs","categorised_FNS_reflection.png"), width = 750, height = 400)
vioplot(categorised_FNS_reflection$DI,categorised_FNS_reflection$DALL,categorised_FNS_reflection$DACC,categorised_FNS_reflection$SYS,categorised_FNS_reflection$other,
        names = c("DI", "DALL","DACC","SYS", "Other"), ylab = "percentage", xlab = "category", col = "gray")
dev.off()


#coverage graph
png(filename=file.path(path,"graphs","branch-coverage.png"), width = 650, height = 400)
vioplot(coverage$coverage_builtin.test,coverage$coverage_generated.test,coverage$coverage_combined,
        names = c("builtin", "generated", "combined"), ylab = "percentage", col = "gray")
dev.off()
