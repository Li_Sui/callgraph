import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

/**
 * Evaluate sample script (regex are becoming too complex ..)
 * @author jens dietrich
 */
public class EvaluateNocallsiteFNsBaseApplicationClasses {
    public static void main (String[] args) throws Exception {
        File f = new File("356Samples-nocallsiteFNs-base-applicationClasses.txt");
        int SAMPLE_COUNT = 0;
        int CLINIT_COUNT = 0;
        int lineNo = 0;
        try (BufferedReader b = new BufferedReader(new FileReader(f))) {
            String line = null;
            boolean newSample = true;
            boolean causedByClint = false;
            while ((line = b.readLine())!=null) {
                lineNo = lineNo + 1;
                if (line.trim().equals("Path from FN CCT root:")) {
                    newSample = true;
                    if (!causedByClint) {
                        System.out.println("Previous example not caused by <clinit>, line: " + lineNo);
                    }
                    causedByClint = false;
                    SAMPLE_COUNT = SAMPLE_COUNT+1;
                }
                else if (newSample && line.contains("<clinit>") && line.endsWith("#nocallsite")) {
                    newSample = false;
                    causedByClint = true;
                    CLINIT_COUNT = CLINIT_COUNT+1;
                }
            }
        }
        System.out.println("Samples analysed: " + SAMPLE_COUNT);
        System.out.println("Samples caused by <clinit>: " + CLINIT_COUNT);
    }
}
