import org.junit.*;
public class Test42 {
  private T tested = null;
  @BeforeClass public void beforeClass() {};
  @Before public void setUp() {tested = new T();}
  @Test public void test() {tested.foo();}	
  @After public void tearDown() {tested = null;}	
  @AfterClass public void afterClass() {};
}
