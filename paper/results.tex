\section{Results}

\subsection{Overview}
\label{ssec:results:overview}

We conducted experiments to measure the recall of various static call graph construction techniques with respect to different oracles. This led to a combinatorial explosion in the number of possible experiments: there are three types of static analyses (context-insensitive, context-insensitive with reflection support and context-sensitive), three possible oracles (constructed from built-in, generated and combined test cases), and the  additional parameter whether to run the analysis in library or whole program (super jar) mode. This implies that 18 computationally expensive experiments had to be conducted and reported for each of the \datasetsize programs, making both the execution and reporting challenging.  To deal with this, we prioritised experiments and proceeded as follows. We first measured the recall of the baseline context-insensitive (base) analysis with respect to the oracles provided by built-in, generated and combined test cases, for both the library and the superjar configuration, the results answer RQ1 and are reported in Section~$\ref{ssec:results:rq1}$. The impacts of context sensitivity (RQ2) and reflection support (RQ3) were then assessed and are reported in Sections~\ref{ssec:results:rq2} and \ref{ssec:results:rq3}. False negatives are further investigated in detail in Section~\ref{ssec:results:rq4} in order to answer RQ4. For RQs 2-4, we restricted the experiments to always use the combined set of (generated and built-in) tests, and the library analysis mode.

% Jens: extra subsection would be nice, but uses too much space
%\subsection{Experiment Runtimes}

We report the run times of the respective experiments in Table \ref{tab:performance}.  While the analysis of performance was not one of our research questions, performance did have an impact on our methodology, and matters as it is part of the trade-off that is being made when choosing a static analysis. Note the high cost of running the instrumented tests (not taking into account the already very high cost of generating tests, reported in~\cite{dietrich2017xcorpus}), and of running the static analysis with reflection support, with only \programcountrqthree experiments avoiding time outs (set to \analysistimeout)~\footnote{For the following programs, the analysis with reflection support did not time out: \programlistrqthree} \footnote{The timeout of 6 hours chosen is at the upper end of the time outs used in related work: \cite{judge,li2018precision} -- 90 mins, \cite{li2019understanding,smaragdakis2018defensive,li2018scalability} -- 3 h, \cite{Fourtounis2018Proxies,grech2018shooting} -- 4 h, \cite{grech2018efficient} -- 6 h, \cite{grech2017p} -- 7 h.}. For all experiments, Java 1.8.0\_144-b01 (Java HotSpot(TM) 64-Bit Server VM, build 25.144-b01, mixed mode), running on a Ubuntu OS was used. The heap size of the JVM was set to 16GB for the CCT recording, 256GB for the CCT reduction and 384GB for the static analyses.
% Jens:skipped over some minor details for space reasons

\begin{table}[hb]
	\footnotesize
	\caption{Experiment run times}
	\label{tab:performance}
	\begin{tabular}{|l|lll|}
		\hline
		analysis & programs analysed & median  & max \\ \hline
		CCT recording & \datasetsize & 5h 5mins& 76h 4mins\\
		CCT processing & \datasetsize & 12h & 144h\\
		SCG construction (base, lib)  & \datasetsize & 3mins & 31mins \\
		SCG construction (base, super)  &  \datasetsize & 3mins & 39mins \\
		SCG construction (ctx-sens, lib)  & \datasetsize  & 8mins & 3h 54mins \\
		%SCG construction (ctx-sens, super)  & & & \\
		% Jens: removed
		%SCG construction (refl-lite, lib)  & \datasetsizelitereflection & 1h 34mins & 6h \\
		%SCG construction (refl-lite, super)  & & & \\
		SCG construction (refl, lib)  &  \programcountrqthree  &1h 36mins & 5h 42mins\\
		%SCG construction (refl, super)  & & & \\
		\hline
	\end{tabular}
\end{table}


\subsection{The Recall of Static Program Analysis}
\label{ssec:results:rq1}

To answer RQ1 we measured the recall of the base static analysis. The recall values for the context-insensitive baseline analysis are depicted in Figure~\ref{figure:recall}. While in general the recall values (combined tests, the static analysis uses the lib setup) were high with a median of ~\medianrecall, the ``unsoundness'' gaps were still significant, indicating that the static analysis typically misses around \missedFNs of the known reachable methods. We also computed the recall with respect to the oracles obtained by the built-in and generated tests separately.
The  recall with respect to the oracle obtained with built-in tests was significantly lower (median \medianrecallbuiltin) than the recall obtained using the generated test oracle (median \medianrecallgenerated).
This suggests an interesting characteristic of built-in tests -- they are better in penetrating code that uses dynamic language features than the generated tests. Note that this result was obtained with tests generated with one particular test generation framework -- \evosuite .
The likely explanation is that test case generators (at least \textit{evosuite}) have to deal with similar problems as static analysis to reason about dynamic language features as intended by the programmer.

\begin{figure}[htb]
	\squeezeup
	\begin{center}
		\includegraphics[width=1\columnwidth]{figures/recall-builtin-vs-generated.png}
		\caption{Recall of the base static analysis with respect to oracles obtained by executing built-in, generated and combined tests, for both lib and superjar mode}
		\label{figure:recall}
	\end{center}
	\squeezeup
\end{figure}


%\noindent\fbox{%
%	\parbox{\columnwidth}{%
%RQ1 What is the lower bound for the recall of static call graph construction ?
%    }%
%}

Figure~\ref{figure:recall} also indicates that there is no significant difference between the library and the superjar analysis mode.
%Small variations can be explained by test flakiness} as discussed in Section~\ref{ssec:drivergeneration}.
This indicates that dynamic language features are not used at component boundaries. We note however that there are programming patterns that do exactly this, but none of the programs in the data set uses them. The use of a plugin-like model used in JDBC 4 with service locators is such a model~\cite[Section 9.2.1]{andersen2006jdbc}.

We also investigated whether the false negatives are methods declared in core Java (methods are declared in classes within \texttt{ java.*} packages), extended Java (other official packages that are part of the Java Runtime Library, such as \texttt{javax.*}, \texttt{org.omg.*}), Java private (\texttt{sun.*}, \texttt{com.sun.*}, \texttt{com.oracle.net}) or application-defined (everything else, including application and third-party library packages). The average percentages of false negatives in the respective categories are as follows: 22.25\% Java core, 10.51\% Java extended, 46.50\% Java private and 21.47\% application. The high number of methods defined in Java-private classes stands out, this is consistent with the results of the cause analysis discussed in Section~\ref{ssec:results:rq4}. The number of methods  in application classes missed by the static analysis is still significant.

\subsection{The Impact of Context-Sensitivity}
\label{ssec:results:rq2}

In order to answer RQ2 we measured the recalls with respect to the oracle created by executing all tests for both the base (context-insensitive) analysis and a context-sensitive analysis as described in Section~\ref{sec:methodology:scg}. The results are depicted in the second column of Figure~\ref{figure:rq23}, the median recall is \medianrecallcontextsensitive. It turns out that gaining precision (i.e., eliminating false positives) has very little impact on the recall. I.e. there are very few false negatives that were covered by the false positives of the less precise context-insensitive analysis.

\begin{figure}[htb]
	\squeezeup
	\begin{center}
		\includegraphics[width=1\columnwidth]{figures/recall-base-vs-reflection_combined.png}
		\caption{Recall of base vs context-sensitive analysis and reflection (ref) analysis. The numbers in brackets indicate the size of the data set used, the base analysis data are provided for both the full data set (column 1) and the
		 reduced data set (column 3)}
		\label{figure:rq23}
	\end{center}
	\squeezeup
\end{figure}

\subsection{The Effectiveness of Dynamic Language Feature Support in Static Analysis}
\label{ssec:results:rq3}

To answer RQ3 we compared the recall obtained by the base analysis with the analyses with reflection support being enabled. This allowed us to measure the effectiveness of state-of-the-art  support for reflection and similar dynamic language features. Unfortunately, the additional reasoning \textit{doop} has to perform is resource-intensive and timed out for several programs, as detailed in Table~\ref{tab:performance}. Therefore, the results summarised in columns 3 and 4 in Figure~\ref{figure:rq23} were obtained with a smaller dataset only consisting of  \programcountrqthree programs~\footnote{\programlistrqthree}. In general, the reflection support in \doop is very effective -- the median recall increases significantly  from \medianrecall to \medianrecallreflection.


%\begin{figure}[htb]
%	\squeezeup
%	\begin{center}
%		\includegraphics[width=1\columnwidth]{figures/recall-base-vs-reflection.png}
%		\caption{Recall of base analysis vs analysis with reflection support with respect to the oracle obtained by running combined tests, using lib mode}
%		\label{figure:reflection-support}
%	\end{center}
%	\squeezeup
%\end{figure}

\subsection{Quantifying the Causes of Unsoundness}
\label{ssec:results:rq4}

In order to answer RQ4, we removed tagged vertices from the CCTs and measured the percentage of false negatives (with respect to a given static analysis) still reachable as described in Section~\ref{sec:methodology:classification}. The results for the base analysis are shown in Figure~\ref{figure:recall-cat-base}. This figure summarises the percentages of false negatives that can be explained by the presence of the respective class of language features across the dataset. The figure uses the aggregated  categories, also showing statistical variation. Details are shown in Table \ref{tab:resultdetails}. It turns out that dynamic invocations are only a minor source of false negatives, in particular the presence of \texttt{Method::invoke} can only explain less than 10\% of the false negatives. Invocations triggered by methods invoked by the JVM and different types of dynamic allocations can explain the majority of false negatives. Note that the data set consists mainly of older programs, and features such as lambdas are likely to be under-represented~\footnote{\cite{dietrich2017xcorpus} contains an overview of the language features used by the \xcorpus programs}.  The only programs where we found false negatives caused by dynamic access are \textit{wct-1.5.2} and \textit{guava-21.0}. Other categories that have overall little impact are allocations when objects are deserialised, dynamic proxies, and \texttt{ Unsafe::getObject}.  However, a significant part of programs uses dynamic proxies and \texttt{ Unsafe::getObject}. More generally, we detected at least some usage of each of the features / patterns investigated when executing the programs in the data set.

Table \ref{tab:resultdetails} also contains the detailed classification of the false negatives left when running the static analysis with reflection support. The base analysis for the reduced data set is also included in order to make the base and the reflection data comparable. Figure~\ref{figure:recall-cat-ref} shows the variation of recall values across the data set. We observe that reflection support addresses a significant share of false negatives caused by \texttt{Method::invoke}. It addresses all false negatives caused by dynamic proxies (invocation handlers) in 3/5 programs, and all false negatives caused by allocation via deserialisation (although there were only 2 programs in this category). For the system category, the percentages increase, indicating that \doop reflection support is relatively ineffective for these categories.

\begin{figure}[htb]
	\squeezeup
	\begin{center}
		\includegraphics[width=1\columnwidth]{figures/categorised_FNS_base.png}
		\caption{Causes of false negatives in the base static analysis}
		\label{figure:recall-cat-base}
	\end{center}
	\squeezeup
\end{figure}


\begin{figure}[htb]
	\squeezeup
	\begin{center}
		\includegraphics[width=1\columnwidth]{figures/categorised_FNS_reflection.png}
		\caption{Causes of false negatives in the static analysis with reflection support}
		\label{figure:recall-cat-ref}
	\end{center}
	\squeezeup
\end{figure}

%\begin{figure}[htb]
%	\begin{center}
%		\includegraphics[width=0.8\columnwidth]{figures/RQ4_sunburst_base.png}
%		\caption{Classification of false negatives for the base static analysis \small (values shown are the medians obtained from the results for each program in the data set)}
%		\label{figure:recall-cat-base-details}
%	\end{center}
%	\squeezeup
%\end{figure}
%
%\begin{figure}[htb]
%	\begin{center}
%		\includegraphics[width=0.8\columnwidth]{figures/RQ4_sunburst_reflection.png}
%		\caption{Classification of false negatives for the static analysis with full reflection support}
%		% \floatfoot{the values shown are the medians obtained from the results for each program in the data set}
%		\label{figure:resultsclassification}
%	\end{center}
%	\squeezeup
%\end{figure}


% JENS: spike: try table
% \begin{table}[]
% 	\footnotesize
% 	\caption{Details classification of false negatives for the base analysis of the full dataset, and base and reflection analysis on the partial dataset of \programcountrqthree programs for which reflection analysis succeeded. Format: average (stdev), number of programs with at least one false negative in this category}
% 	\label{tab:resultdetails}
% 	\begin{tabular}{|l|l|l|l|}
% 		\hline
% 		category (tag)         & base (31)           & base (20)         & reflection       \\ 	\hline
% 		\#method.invoke        & 8.08 (8.88)\%, 29   & 7.14 (8.44)\%, 18 & 4.22 (4.53)\%, 16\\
% 		\#lambda                & 0.10 (0.16)\%, 16   & 0.10 (0.18)\%, 10 & 0.01 (0.04)\%, 1 \\
% 		\#handler.invoke       & 1.45 (1.98)\%, 24   & 1.73 (2.42)\%, 14 & 2.98 (4.13)\%, 14\\
% 		\#dynproxy.invoke      & 0.42 (0.80)\%, 14   & 0.21 (0.56)\%, 5  & 0.26 (0.81)\%, 2 \\
% 		\#class.newinstance    & 21.36 (14.31)\%, 29 & 19.62(14.12)\%, 18& 20.64(17.03)\%, 18\\
% 		\#constr.newinstance   & 5.97 (6.41)\%, 28   & 4.76 (5.53)\%, 17 & 5.61 (6.82)\%, 17\\
% 		\#deserialize          & 0.01 (0.06\%), 3    & 0.02 (0.08)\%, 2  & 0 (0)\%, 0\\
% 		\#unsafe.getobject     & 0.15 (0.30)\%, 9    & 0.18 (0.35)\%, 6  & 0.27 (0.52)\%, 6\\
% 		\#nativeallocation     & 40.00 (12.67)\%, 31 & 41.81(12.70)\%, 20& 36.24 (14.41)\%, 20\\
% 		\#field.get            & 0.08 (0.40)\%, 2    & 0 (0)\%, 0        & 0 (0)\%, 0\\
% 		\#nocallsite           & 76.00 (12.27)\%, 31 & 74.64(11.50)\%, 20& 76.28 (12.54)\%, 20\\
% 		\#systemthread         & 2.57 (3.25)\%, 31   & 3.41 (3.80)\%, 20 & 4.82 (4.88)\%, 20\\
% 		other                  & 8.14 (6.91)\%, 31   & 7.52 (5.49)\%, 20 & 6.35 (7.83)\%, 20\\        \hline
% 	\end{tabular}
% \end{table}
% JENS: spike ends

\begin{table}[]
\footnotesize
\caption{Detailed classification of false negatives for the base
analysis of the full dataset, and the base and reflection analysis
on the partial dataset of 20 programs for which reflection
analysis succeeded  in percent (\# - number of programs with more than one false negative in this category).}
\label{tab:resultdetails}
\resizebox{\columnwidth}{!}{%
\begin{tabular}{|l|lll|lll|lll|}
\hline
\multirow{2}{*}{category (tag)} & \multicolumn{3}{c|}{base (31)} & \multicolumn{3}{c|}{base (20)} & \multicolumn{3}{c|}{reflection} \\ \cline{2-10}
 & avg & stdev & \# & avg & stdev & \# & avg & stdev & \# \\ \hline
\#method.invoke & 8.08 & 8.88 & 29 & 7.14 & 8.44 & 18 & 4.22 & 4.53 & 16 \\
\#lambda & 0.10 & 0.16 & 16 & 0.10 & 0.18 & 10 & 0.01 & 0.04 & 1 \\
\#handler.invoke & 1.45 & 1.98 & 24 & 1.73 & 2.42 & 14 & 2.98 & 4.13 & 14 \\
\#dynproxy.invoke & 0.42 & 0.80 & 14 & 0.21 & 0.56 & 5 & 0.26 & 0.81 & 2 \\ \hline
\#class.newinstance & 21.36 & 14.31 & 29 & 19.62 & 14.12 & 18 & 20.64 & 17.03 & 18 \\
\#constr.newinstance & 5.97 & 6.41 & 28 & 4.76 & 5.53 & 17 & 5.61 & 6.82 & 17 \\
\#deserialize & 0.01 & 0.06 & 3 & 0.02 & 0.08 & 2 & 0 & 0 & 0 \\
\#unsafe.getobject & 0.15 & 0.30 & 9 & 0.18 & 0.35 & 6 & 0.27 & 0.52 & 6 \\
\#nativeallocation & 40.00 & 12.67 & 31 & 41.81 & 12.7 & 20 & 36.24 & 14.41 & 20 \\ \hline
\#field.get & 0.08 & 0.40 & 2 & 0 & 0 & 0 & 0 & 0 & 0 \\ \hline
\#nocallsite & 52 & 22.51 & 31 & 47.27 & 19.34 & 20 & 52.49 & 20.57 & 20 \\
\#systemthread & 2.57 & 3.25 & 31 & 3.41 & 3.8 & 20 & 4.82 & 4.88 & 20 \\ \hline
other & 17.94 & 11.39 & 31 & 17.96 & 11.14 & 20 & 13.83 & 10.74 & 20 \\ \hline
\end{tabular}}
\end{table}


We then analysed the percentage of false negatives left after all tagged vertices were removed from the CCTs, this is the number of uncategorised false negatives presented in the last columns labelled \textit{Other} in Figures~\ref{figure:recall-cat-base} and \ref{figure:recall-cat-ref}. It represents the (in)completeness of our automated cause analysis.
% The median value of this category is at 12.6\% (reflection).  // jens: removed this as data is in table for all the different analyses
%Also note the percentages from the respective categories add up to more than 100 for reasons discussed in at the end of Section~\ref{sec:methodology:classification}.
%This may seem counter-intuitive at first, but simply means that there is more than one dynamic language feature used in order to reach a call site. Examples are the use of reflection in a system thread, or the invocation of a reflective methods on an object that was dynamically allocated.
We manually analysed a sample of false negatives in the \textit{Other} category, aiming for a confidence level of 95\% and a confidence interval of 5\%. This yielded a sampling size of 373 for the base and 344 for the reflection analysis. We then randomly extracted the respective number of CCT paths from the respective CCT root to an uncategorised false negative, and inspected them. It turns out that they are dominated by a single pattern we refer to as \textit{double-reflective factory}, which we discuss in some more detail next. This accounted for 54.4\% of the uncategorised false negatives in the base analysis and 51.5\% of the uncategorised false negatives in the analysis with reflection support. There are some other patterns we detected, we omit the detailed discussion for space reasons.

The double-reflective factory is a particular use of the factory design pattern~\cite{johnson1995design} in conjunction with reflection, used to manage character sets. To illustrate this, consider the stack trace caused by an invocation of \texttt{System.out.\-print\-ln()} in Listing~\ref{code:sysout}. Using both the base and the reflection analysis, \texttt{encode\-Loop} is unreachable in the statically computed call graph. The encoder is created by the \texttt{Charset} (\texttt{sun.nio.cs.UTF}) which is created via reflection (\texttt{Class::forName} and \texttt{Class::newInstance}) by a \texttt{CharsetProvider} (\texttt{sun.nio.cs.\-Fast\-CharsetProvider}) which is in fact itself also created using reflection by a service loader from jar manifest meta data. This is a triple factory, with two of the factories using reflective allocation. This is a good example of framework complexity Java is known for. While our analysis tags the factories as dynamically allocated, it does not do this to the objects created in those factories using plain object allocation with \texttt{new}. While an extension of our mechanism to cover such cases is possible,  we decided not to do this in the scope of this work as our cause analysis had reached a coverage we considered as sufficient to confidently answer RQ4.

\lstinputlisting[language=Java,label=code:sysout,caption=Stacktrace created by the invocation of \texttt{PrintStream::println}]{system-out-println.java}

We also sampled the \textit{nocallsite} and \textit{systemthread} categories, focusing on false negatives defined in applications or third-party libraries.  It turns out that for the base analysis, 9/\datasetsize programs have such false negatives in the \textit{systemthread} category, and 28/\datasetsize programs have such false negatives in the \textit{nocallsite} category. Using the same sampling procedure as described above, we found that 86\% of the application false negatives in the \textit{nocallsite} category are caused by static initialisers (\texttt{<clinit> }methods) invoked by the JVM. Another example are invocations of \texttt{Runnable::run} methods through native dispatch from \texttt{java.security.Access\-Controller::do\-Privileged}. Reflective method invocations are also classified in this category due to the native dispatch in \texttt{sun.re\-flect.NativeMethod\-Accessor\-Impl\#invoke0},  in the sampling set, this accounted for 4.8\% of cases. Sampling application-defined methods classified as \textit{systemthread} reveals that those can all be explained by invocations of \texttt{finalize} in application classes in the \texttt{Finalizer} thread.


\subsection{Threats to Validity}

The test unreflection process described in Section~\ref{ssec:drivergeneration} has limitations as \junit features such as tests with rules and custom runners were ignored, i.e., not unreflected. This has reduced the coverage of the oracle.

Test flakiness~\cite{luo2014empirical} is a known issue that affects test outcomes, including coverage. We found small variations in test coverage in \programcountwithflakytests of the \datasetsize programs between executions. Figure~\ref{figure:coverage} uses averages from five runs, the oracle used was generated by a single run. The reason for this decision is the  high cost of oracle generation (see Table~\ref{tab:performance}). In some cases, a slightly larger oracle could have been obtained by running the (instrumented) tests multiple times, and merging the constructed CCTs.

The classification method discussed in Section~\ref{sec:methodology:classification} can explain most, but not all false negatives. We addressed this by sampling and manual analysis.

The tagging of lambdas relies on naming patterns used by the OpenJDK compiler. There is a possibility that some of the library code within the analysis scope has been compiled with a different compiler using a different convention. This would have resulted in more false negatives not being classified, as mentioned  above, this was addressed by sampling.

Tagging with  \texttt{\#nocallsite} relied on a static pre-analysis to collect the call sites in methods. For libraries, this depends on the library version used. There is a chance that in some cases programs use custom class loaders, choosing a different version of the class. This would have resulted in methods being  incorrectly tagged as  \texttt{\#nocallsite}, and in an over-reporting of false negatives in this category.
