public class Driver_Test42 {
  public static void main(String[] args) throws Exception {
    Test42 test = new Test42();
    test.beforeClass();
    test.setup(); 
    test.test();
    test.tearDown();
    test.afterClass();
  }
}
