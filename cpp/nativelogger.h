
#include <jni.h>


#ifndef _Included_nz_ac_massey_cs_instrumentation_NativeLogger
#define _Included_nz_ac_massey_cs_instrumentation_NativeLogger
#ifdef __cplusplus
extern "C" {
#endif
JNIEXPORT jstring JNICALL Java_nz_ac_massey_cs_instrumentation_NativeLogger_getInvocationID
  (JNIEnv *, jobject);

JNIEXPORT void JNICALL Java_nz_ac_massey_cs_instrumentation_NativeLogger_push
  (JNIEnv *, jobject,jstring,jstring,jstring,jstring,jstring,jint,jint,jlong,jstring,jint);


JNIEXPORT void JNICALL Java_nz_ac_massey_cs_instrumentation_NativeLogger_pop
  (JNIEnv *, jobject,jint,jlong);


JNIEXPORT void JNICALL Java_nz_ac_massey_cs_instrumentation_NativeLogger_flush
  (JNIEnv *, jobject);


JNIEXPORT void JNICALL Java_nz_ac_massey_cs_instrumentation_NativeLogger_clear
  (JNIEnv *, jobject,jstring,jstring,jstring,jstring,jstring,jint,jlong);

JNIEXPORT void JNICALL Java_nz_ac_massey_cs_instrumentation_NativeLogger_addAllocationHash
  (JNIEnv *, jobject,jint,jint);


#ifdef __cplusplus
}
#endif
#endif
