#!/bin/bash
if [ $# -eq 0 ]; then
    echo >&2 "Usage: jstackSeries <pid> <run_user> [ <count> [ <delay> ] ]"
    echo >&2 "    Defaults: count = 10, delay = 0.5 (seconds)"
    exit 1
fi
count=${1:-10}  # defaults to 10 times
delay=${2:-0.5} # defaults to 0.5 seconds
while [ $count -gt 0 ]
do
	jps -q | xargs -L1 jstack >jstack.$(date +%H%M%S.%N)
    sleep $delay
    let count--
    echo -n "."
done
