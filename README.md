# A Tool to Harvest call-graph Dynamically

This project is inspired by [Georgios Gousios's java-callgraph in Github](https://github.com/gousiosg/java-callgraph). We aim the completeness of call graph which means call edges in JDK are recorded as well. 
The tool has been using by the paper: **[Li, Dietrich, Tahir and Fourtounis: On the Recall of Static Call Graph Construction in Practice (ICSE'20)](https://ecs.wgtn.ac.nz/foswiki/pub/Main/JensDietrich/recall.pdf)**

Here are two problems instrumenting all loaded classes

1. instrumenting all methods may end up in a loop. e.g. the advice gets called, calls back into the type that advised, which in turns calls back into the advice. We created a logger in native code.
2. native method. e.g. Thread.start(); -> Thread.start0() We implemented a pre-analysis to gather all native call sites


## Handling Thread and Exception


* Thread: each thread has a unique method stack. We use System.identityHashCode(Thread.currentThread()) to differentiate thread objects
* Exception: insert a statement to clear the stack up to the current method. e.g.

    ```
    catch(Exception e){

      clear();// inserted during the instrumenation

      method();
    }
    ```


## Class identifier

In multi-classloader environments the plain name of a class does not unambiguously identify a class. [Ref](https://www.jacoco.org/jacoco/trunk/doc/implementation.html)

The information of current class loader is concatenate with method name when push onto the stack.
```
sun.misc.Launcher$AppClassLoader@18b4aac2+com.example.Class+method+()V
```

## How to build

The project can be built with Maven. `mvn clean package`

To compile C++ in Linux:

```
g++ -std=c++11 -shared -fPIC -I$JAVA_HOME/include -I$JAVA_HOME/include/linux cpp/nativelogger.cpp -o nativelogger.so
```

To collect all native callsites
```
java nz.ac.massey.cs.preanalysis.NativeMethodFinder [Dir of jars] [path of rt.jar] [output file]
```

To generate call graph. Note that the Instrumentation only works on jre1.8.**

```
java -Xbootclasspath/p:target/dcg-0.1-SNAPSHOT-instrumentation.jar -javaagent:target/dcg-0.1-SNAPSHOT-instrumentation.jar=nativeCallsite.txt -cp target/dcg-0.1-SNAPSHOT-tests.jar nz.ac.massey.cs.example.SimpleTest
```


## Limitation

There are some noises produced by the agent.  Instrumentation.retransformClasses() re-transforms classes that already loaded by JVM. This retransformation triggers transform() method to inject code and installed as the new definition of the class.
 - re-transformation transforms classes one by one.
 - the instrumentation has calls into runtime API (e.g. java.util.LinkedList) which has been transformed and installed.
 - Javaassit has calls into runtime API which has been transformed and installed.

To deal with it, we apply filter to remove disconnect graphs(assume program entry is ....main(String[] args))

