public class Main {
	private static void fooClinit(){}

	static {
		fooClinit();
	}

	public static void main(String[] args) {
		fooMain();
	}

	private static void fooMain(){}

	@Override
	public void finalize() {
		fooFinalize();
	}

	private void fooFinalize() {}
}