## Analyses

1. `./doop --platform java_8 -a context-insensitive -i foo.jar.`
2. `./doop --platform java_8 -a context-insensitive -i foo.jar -main Main`

## Analysed Program

Note: program has been packaged as `foo.jar`.

```
public class Main {
	private static void fooClinit(){}

	static {
		fooClinit();
	}
	public static void main(String[] args) {
		fooMain();
	}
	private static void fooMain(){}
	@Override public void finalize() {
		fooFinalize();
	}
	private void fooFinalize() {}
}
```

## Reachability

R - reachable (Reachable.csv)
E - edge recorded (CallGraphEdge.csv)

| analysis    | fooMain()   | fooFinalize() |  fooClinit() |
| ----------- | ----------- | ------------- | ------------ |
| 1           | R,E         | R,E           | R,E          |
| 2 `(-main)` | R,E         | -             | R,E          |
 
