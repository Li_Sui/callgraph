package nz.ac.massey.cs.resultAnalysis;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.tuple.Pair;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.nio.charset.Charset;
import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Stream;

public class RQ4Sunburst {
    //match exact java.lang.reflect.Method.invoke
    public static boolean isReflectionInvoke(Method method){
        return method.equals(Method.METHOD_INVOKE);
    }
    //match all java.lang.invoke.MethodHandle.invoke*
    public static boolean isMethodHandleInvoke(Method method){
        return method.getClassName().equals("java.lang.invoke.MethodHandle") && method.getName().startsWith("invoke");
    }
    //match invokedynamic
    public static boolean isInvokedynamic(Method method){
        return method.getName().contains("lambda$");
    }
    //match all InvocationHandler.invoke
    public static boolean isPorxyInvoke(Method method){
        return method.getDescriptor().equals("(Ljava/lang/Object;Ljava/lang/reflect/Method;[Ljava/lang/Object;)Ljava/lang/Object;") && method.getName().equals("invoke");
    }

    //match invocation
    public static boolean isInvocation(Method method){
        return isReflectionInvoke(method) || isMethodHandleInvoke(method) || isPorxyInvoke(method) || isInvokedynamic(method);
    }
    //match dynamic allocations
    public static boolean isAllocation(String tag){
        return isClassNewInstance(tag) || isConstructorNewInstance(tag) || isSerialisation(tag) || isUnsafe(tag) || isNativeReturn(tag);
    }
    public static boolean isClassNewInstance(String tag){
        return tag.equals("Class.newInstance");
    }
    public static boolean isConstructorNewInstance(String tag){
        return tag.equals("Constructor.newInstance");
    }
    public static boolean isSerialisation(String tag){
        return tag.equals("ObjectInputStream.readObject");
    }
    public static boolean isUnsafe(String tag){
        return tag.equals("Unsafe.getObject");
    }
    public static boolean isNativeReturn(String tag){
        return tag.equals("native.return");
    }

    //match Field.get
    public static boolean isDynamicAccess(String tag){
        return tag.equals("Field.get");
    }

    public static boolean isImplicit(String tag, String threadName){
        return matchNoSuchCallSite(tag) || matchSystemThread(threadName);
    }
    //match no such call site in a give list
    public static boolean matchNoSuchCallSite(String tag){
        return tag.equals("nocallsite");
    }
    //match already known system threads
    public static boolean matchSystemThread(String threadName){
        List<String> systemThread= Arrays.asList("AWT-EventQueue-0","AWT-Shutdown","Finalizer","DestroyJavaVM","Reference Handler","Signal Dispatcher");
        return systemThread.contains(threadName);
    }

    public static boolean matchAllCategories(Method method,String tag,String threadName){
        return isImplicit(tag,threadName) || isDynamicAccess(tag) || isAllocation(tag) || isInvocation(method);
    }
    static String OUTPUTHEADER="program, reflection invoke, handler invoke, invokedynamic, proxy invoke, classNewInstance, constructorNewInstance ,serialisation, unsafe, native return,field get,nosuchcallsite,system thread\n";
    public static void main(String[] args) throws Exception{
        Options options = new Options()
                .addOption("inputDir", true, "processed-CCTs-data folder containing all dataset, including programs, CCTs,FNs and invocation list")
                .addOption("outputDir",true,"output folder");
        CommandLine cmd = new DefaultParser().parse(options, args);

        if (!cmd.hasOption("outputDir")|| !cmd.hasOption("inputDir")) {
            HelpFormatter formatter = new HelpFormatter();
            formatter.printHelp("java -cp <classpath> <options> " + RQ4Sunburst.class.getName(), options);
            System.exit(0);
        }

        String inputDir=cmd.getOptionValue("inputDir");
        String outputDir=cmd.getOptionValue("outputDir");
        StringBuilder sb =new StringBuilder(OUTPUTHEADER);
        for(File f: new File(inputDir).listFiles()) {
            if(f.isDirectory()) {
                String program = f.getName();
                sb.append(program + ",");
                String analysis = "contextInsensitive";
                Set<String> combinedFNs_ref = new HashSet<>();
                Set<String> bt_fn_ref = getFNSet(new File(inputDir+ "/" + program + "/FNs-builtinTest/FNs-" + analysis + "-lib.csv"));
                Set<String> gt_fn_ref = getFNSet(new File(inputDir+ "/" + program + "/FNs-generatedTest/FNs-" + analysis + "-lib.csv"));
                combinedFNs_ref.addAll(bt_fn_ref);
                combinedFNs_ref.addAll(gt_fn_ref);
                int combinednoFns = combinedFNs_ref.size();
                processFNs(combinednoFns, inputDir + "/" + program, sb, analysis);
                sb.append("\n");
            }
        }

        IOUtils.write(sb.toString(),new FileOutputStream(new File(outputDir+"/RQ4-base.csv")), Charset.defaultCharset());
        sb=new StringBuilder(OUTPUTHEADER);
        for(File f: new File(inputDir).listFiles()) {
            if(f.isDirectory()) {
                String program = f.getName();
                String analysis = "reflection";
                Set<String> combinedFNs_ref = new HashSet<>();
                Set<String> bt_fn_ref = getFNSet(new File(inputDir + "/" + program + "/FNs-builtinTest/FNs-" + analysis + "-lib.csv"));
                Set<String> gt_fn_ref = getFNSet(new File(inputDir + "/" + program + "/FNs-generatedTest/FNs-" + analysis + "-lib.csv"));
                combinedFNs_ref.addAll(bt_fn_ref);
                combinedFNs_ref.addAll(gt_fn_ref);
                int combinednoFns = combinedFNs_ref.size();
                processFNs(combinednoFns, inputDir + "/" + program, sb, analysis);
                sb.append("\n");
            }
        }
        IOUtils.write(sb.toString(),new FileOutputStream(new File(outputDir+"/RQ4-reflection.csv")), Charset.defaultCharset());
    }

    public static synchronized void processFNs(int combinednoFns,String programDir,StringBuilder sb,String analysis) throws Exception{
        String fn_bt=programDir+"/FNs-builtinTest/FNs-"+analysis+"-lib.csv";
        String fn_gt=programDir+"/FNs-generatedTest/FNs-"+analysis+"-lib.csv";

        String recordings_bt=programDir+"/CCTs-builtinTest";
        String recordings_gt=programDir+"/CCTs-generatedTest";

        //category: invocation
        Set<Method> combined_reflection_invoke=new HashSet<>();
        combined_reflection_invoke.addAll(getReflectionInvokeFNs(fn_bt,recordings_bt));combined_reflection_invoke.addAll(getReflectionInvokeFNs(fn_gt,recordings_gt));
        Set<Method> combined_handler_invoke=new HashSet<>();
        combined_handler_invoke.addAll(getMethodHandleInvokeFNs(fn_bt,recordings_bt));combined_handler_invoke.addAll(getMethodHandleInvokeFNs(fn_gt,recordings_gt));
        Set<Method> combined_invokedynamic=new HashSet<>();
        combined_invokedynamic.addAll(getInvokedynamicFNs(fn_bt,recordings_bt));combined_invokedynamic.addAll(getInvokedynamicFNs(fn_gt,recordings_gt));
        Set<Method> combined_proxy_invoke=new HashSet<>();
        combined_proxy_invoke.addAll(getPorxyInvokeFNs(fn_bt,recordings_bt)); combined_proxy_invoke.addAll(getPorxyInvokeFNs(fn_gt,recordings_gt));
        //category: allocation
        Set<Method> combined_classNewInstance=new HashSet<>();
        combined_classNewInstance.addAll(getClassNewInstanceFNs(fn_bt,recordings_bt));combined_classNewInstance.addAll(getClassNewInstanceFNs(fn_gt,recordings_gt));
        Set<Method> combined_constructorNewInstance=new HashSet<>();
        combined_constructorNewInstance.addAll(getConstructorNewInstanceFNs(fn_bt,recordings_bt));combined_constructorNewInstance.addAll(getConstructorNewInstanceFNs(fn_gt,recordings_gt));
        Set<Method> combined_serialisation=new HashSet<>();
        combined_serialisation.addAll(getSerialisationFNs(fn_bt,recordings_bt));combined_serialisation.addAll(getSerialisationFNs(fn_gt,recordings_gt));
        Set<Method> combined_unsafe=new HashSet<>();
        combined_unsafe.addAll(getUnsafeFNs(fn_bt,recordings_bt));combined_unsafe.addAll(getUnsafeFNs(fn_gt,recordings_gt));
        Set<Method> combined_nativereturn=new HashSet<>();
        combined_nativereturn.addAll(getNativeReturnFNs(fn_bt,recordings_bt));combined_nativereturn.addAll(getNativeReturnFNs(fn_gt,recordings_gt));
        //category: dynamic access
        Set<Method> combined_fieldGet=new HashSet<>();
        combined_fieldGet.addAll(getDynAccessFNs(fn_bt,recordings_bt));combined_fieldGet.addAll(getDynAccessFNs(fn_gt,recordings_gt));
        //category: implicit
        Set<Method> combined_nosuchcallsite=new HashSet<>();
        combined_nosuchcallsite.addAll(getNoSuchCallsiteFNs(fn_bt,recordings_bt));
        combined_nosuchcallsite.addAll(getNoSuchCallsiteFNs(fn_gt,recordings_gt));
        Set<Method> combined_systemThread=new HashSet<>();
        combined_systemThread.addAll(getSystemTheadFNs(fn_bt,recordings_bt));combined_systemThread.addAll(getSystemTheadFNs(fn_gt,recordings_gt));


        double reflection_invoke_FNs= Double.valueOf(combined_reflection_invoke.size())/Double.valueOf(combinednoFns);
        double handler_invoke_FNs= Double.valueOf(combined_handler_invoke.size())/Double.valueOf(combinednoFns);
        double invokedynamic_FNs= Double.valueOf(combined_invokedynamic.size())/Double.valueOf(combinednoFns);
        double proxy_invoke_FNs= Double.valueOf(combined_proxy_invoke.size())/Double.valueOf(combinednoFns);

        double classNewInstance_FNs= Double.valueOf(combined_classNewInstance.size())/Double.valueOf(combinednoFns);
        double constructorNewInstance_FNs= Double.valueOf(combined_constructorNewInstance.size())/Double.valueOf(combinednoFns);
        double serialisation_FNs= Double.valueOf(combined_serialisation.size())/Double.valueOf(combinednoFns);
        double unsafe_FNs= Double.valueOf(combined_unsafe.size())/Double.valueOf(combinednoFns);
        double nativereturn_FNs= Double.valueOf(combined_nativereturn.size())/Double.valueOf(combinednoFns);
        double fieldGet_FNs= Double.valueOf(combined_fieldGet.size())/Double.valueOf(combinednoFns);
        double nosuchcallsite_FNs= Double.valueOf(combined_nosuchcallsite.size())/Double.valueOf(combinednoFns);
        double systemThread_FNs= Double.valueOf(combined_systemThread.size())/Double.valueOf(combinednoFns);



        sb.append(reflection_invoke_FNs+","+handler_invoke_FNs+","+invokedynamic_FNs+","+proxy_invoke_FNs+","+classNewInstance_FNs+","+constructorNewInstance_FNs+","+serialisation_FNs+","+unsafe_FNs+","
        +nativereturn_FNs+","+fieldGet_FNs+","+nosuchcallsite_FNs+","+systemThread_FNs);
    }

    public static Set<Method> getClassNewInstanceFNs(String fnFile,String recordings) throws Exception{
        String[] arg= {"-fn",fnFile,"-recordings",recordings};
        Predicate<Invocation> filter = inv -> !isClassNewInstance(inv.getTag());
        return getRemovedFNs(filter ,arg);
    }


    public static Set<Method> getConstructorNewInstanceFNs(String fnFile,String recordings) throws Exception{
        String[] arg= {"-fn",fnFile,"-recordings",recordings};
        Predicate<Invocation> filter = inv -> !isConstructorNewInstance(inv.getTag());
        return getRemovedFNs(filter ,arg);
    }

    public static Set<Method> getSerialisationFNs(String fnFile,String recordings) throws Exception{
        String[] arg= {"-fn",fnFile,"-recordings",recordings};
        Predicate<Invocation> filter = inv -> !isSerialisation(inv.getTag());
        return getRemovedFNs(filter ,arg);
    }

    public static Set<Method> getUnsafeFNs(String fnFile,String recordings) throws Exception{
        String[] arg= {"-fn",fnFile,"-recordings",recordings};
        Predicate<Invocation> filter = inv -> !isUnsafe(inv.getTag());
        return getRemovedFNs(filter ,arg);
    }


    public static Set<Method> getNativeReturnFNs(String fnFile,String recordings) throws Exception{
        String[] arg= {"-fn",fnFile,"-recordings",recordings};
        Predicate<Invocation> filter = inv -> !isNativeReturn(inv.getTag());
        return getRemovedFNs(filter ,arg);
    }


    public static Set<Method> getReflectionInvokeFNs(String fnFile,String recordings) throws Exception{
        String[] arg= {"-fn",fnFile,"-recordings",recordings};
        Predicate<Invocation> filter = inv -> !isReflectionInvoke(inv.getMethod());
        return getRemovedFNs(filter ,arg);
    }

    public static Set<Method> getMethodHandleInvokeFNs(String fnFile,String recordings) throws Exception{
        String[] arg= {"-fn",fnFile,"-recordings",recordings};
        Predicate<Invocation> filter = inv -> !isMethodHandleInvoke(inv.getMethod());
        return getRemovedFNs(filter ,arg);
    }

    public static Set<Method> getInvokedynamicFNs(String fnFile,String recordings) throws Exception{
        String[] arg= {"-fn",fnFile,"-recordings",recordings};
        Predicate<Invocation> filter = inv -> !isInvokedynamic(inv.getMethod());
        return getRemovedFNs(filter ,arg);
    }

    public static Set<Method> getPorxyInvokeFNs(String fnFile,String recordings) throws Exception{
        String[] arg= {"-fn",fnFile,"-recordings",recordings};
        Predicate<Invocation> filter = inv -> !isPorxyInvoke(inv.getMethod());
        return getRemovedFNs(filter ,arg);
    }



    public static Set<Method> getDynAccessFNs(String fnFile,String recordings) throws Exception{

        //remove branches that caused by dynamic access
        Predicate<Invocation> dynamicAccess = inv -> !isDynamicAccess(inv.getTag());
        String[] arg= {"-fn",fnFile,"-recordings",recordings};

        return getRemovedFNs(dynamicAccess,arg);
    }
    public static Set<Method> getSystemTheadFNs(String fnFile,String recordings) throws Exception{
        String[] arg= {"-fn",fnFile,"-recordings",recordings};
        //remove branches that caused by invocation without such callsite
        Predicate<Invocation> filter = inv -> !matchSystemThread(inv.getThreadName());

        return getRemovedFNs(filter,arg);
    }

    public static Set<Method> getNoSuchCallsiteFNs(String fnFile,String recordings) throws Exception{
        String[] arg= {"-fn",fnFile,"-recordings",recordings};

        //remove branches that caused by invocation without such callsite
        Predicate<Invocation> filter = inv -> !matchNoSuchCallSite(inv.getTag());

        return getRemovedFNs(filter,arg);
    }

    public static Set<Method> getRemovedFNs(Predicate<Invocation> filter, String[] arg) throws Exception{
        Pair<Set<Method>, Stream<CallTree>> data = IO.importDataForProcessing(arg);
        Set<Method> fnsDetectedAfterFiltering = Collections.synchronizedSet(new HashSet<>());
        Set<Method> fns=data.getLeft();
        data.getRight()
                .forEach(
                        callTree -> {
                            callTree.dfsParallel(filter, inv -> {
                                if (inv.isFN()) {
                                    fnsDetectedAfterFiltering.add(inv.getMethod());
                                }
                            });
                        }
                );
        fns.removeAll(fnsDetectedAfterFiltering);
        return fns;
    }

    private static Set<String> getFNSet(File file){
        Set<String> fns =new HashSet<>();

        try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
            String line = null;
            while ((line = reader.readLine()) != null) {
                fns.add(line);
            }
        }catch (Exception e){e.printStackTrace();}

        return fns;
    }

    public static String buildMethod(Method m){
        return m.getClassName()+","+m.getName()+","+m.getDescriptor();
    }
}
