package nz.ac.massey.cs.resultAnalysis;

import com.google.common.base.Preconditions;
import com.google.common.collect.*;

import java.io.PrintStream;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class CallTree  {

    public static AtomicInteger edgeCount = new AtomicInteger();

    private Invocation root = null;
    private String threadID =  null;
    private String dataSource = null;

    public CallTree(String thread, String dataSource) {
        Preconditions.checkNotNull(thread);
        Preconditions.checkNotNull(dataSource);
        this.threadID = thread;
        this.dataSource = dataSource;
    }

    public String getThreadID() {
        return threadID;
    }

    public String getDataSource() {
        return dataSource;
    }

    public Invocation getRoot() {
        return root;
    }

    public boolean hasRoot() {
        return root!=null;
    }

    public void setRoot(Invocation root) {
        this.root = root;
    }

    public void addEdge(Invocation from,Invocation to) {
        assert root!=null;
        boolean success = from.addChild(to);
        assert success;
    }

    public void printBranch(Invocation branchRoot) {
        printBranch(branchRoot,System.out,0);
    }

    private void printBranch(Invocation invk, PrintStream out, int depth) {
        for (int i=0;i<depth;i++) {
            out.print(" ");
        }
        out.print(invk.toString());
        for (Invocation next:invk.getChildren()) {
            printBranch(next,out,depth+1);
        }
    }

    public Set<Method> getFNs() {
        return getInvocationsOfFNs().stream().map(inv -> inv.getMethod()).collect(Collectors.toSet());
    }

    public Collection<Invocation> getInvocationsOfFNs() {
        Collection<Invocation> fns = new HashSet<>();
        dfs (inv -> true, inv -> {
            if (inv.isFN()) {
                fns.add(inv);
            }
        });
        return fns;
    }

    public int depth() {
        final AtomicInteger depth = new AtomicInteger();
        dfs (inv -> true, inv -> {
            if (inv.getDepth()>depth.intValue()) {
                depth.set(inv.getDepth());
            }
        });
        return depth.intValue();
    }


    // Tests whether an invocation has a ancestor satisfying a condition.
    // If so, a path to the ancestor is returned, null otherwise.
    public List<Invocation> findAncestorSuchThat(Invocation invocation, Predicate<Invocation> test) {
        List<Invocation> reversePath = new ArrayList<>();
        reversePath.add(invocation);
        if (test.test(invocation)) return reversePath;

        Invocation parent = invocation.getParent();

        List<Invocation> parentPath = findAncestorSuchThat(parent,test);
        if (parentPath==null) {
            return null;
        }
        else {
            reversePath.addAll(parentPath);
            return reversePath;
        }
    }

    public Set<PivotalInvocation> findPivotalFNInvocations () {

        Map<Invocation, PivotalInvocation> cache = new ConcurrentHashMap<>();
        cache.put(root, PivotalInvocation.NIL);

        return this.getInvocationsOfFNs().parallelStream()
            .map(inv -> computePivotalInvocation(inv,cache))
            .collect(Collectors.toSet());
    }

    private PivotalInvocation computePivotalInvocation(Invocation inv,Map<Invocation, PivotalInvocation> cache) {
        PivotalInvocation pivot = cache.get(inv);
        if (pivot==null) {
            Invocation parent = inv.getParent();
            assert parent!=null; // cache is pre-initialised with root
            PivotalInvocation parentPivot = computePivotalInvocation(parent,cache);
            if (parentPivot==PivotalInvocation.NIL && inv.isFN()) {
                pivot = new PivotalInvocation(this,inv,parent);
            }
            else {
                pivot = parentPivot;
            }
            cache.put(inv,pivot);
        }
        return pivot;
    }


    public List<Invocation> findPathToRootFrom(Invocation invocation) {
        List<Invocation> path = findAncestorSuchThat(invocation,inv -> inv==root);
        assert path!=null;
        return path;
    }

    public List<Invocation> findPathFromRootTo(Invocation invocation) {
        List<Invocation> path = findPathToRootFrom(invocation);
        assert path!=null;
        return Lists.reverse(path);
    }

    public Set<Invocation> getInvocations() {
        Set<Invocation> invocations = new HashSet<>();
        dfs(inv -> true,inv -> {
            invocations.add(inv);
        });
        return  invocations;
    }

    public int size () {
        return getInvocations().size();
    }

    public void dfs (Predicate<Invocation> traverseFilter, Consumer<Invocation> action) {
        dfs(traverseFilter,action,this.root);
    }

    private void dfs (Predicate<Invocation> traverseFilter, Consumer<Invocation> action,Invocation node) {
        if (traverseFilter.test(node)) {
            action.accept(node);
            for (Invocation next:node.getChildren()) {
                dfs(traverseFilter,action,next);
            }
        }
    }

    public void dfsParallel (Predicate<Invocation> traverseFilter, Consumer<Invocation> action) {
        dfsParallel(traverseFilter,action,this.root);
    }

    private void dfsParallel (Predicate<Invocation> traverseFilter, Consumer<Invocation> action,Invocation node) {
        if (traverseFilter.test(node)) {
            action.accept(node);
            node.getChildren().parallelStream().forEach(
                next -> dfsParallel(traverseFilter, action, next)
            );
        }
    }



    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CallTree callTree = (CallTree) o;

        if (!threadID.equals(callTree.threadID)) return false;
        return dataSource.equals(callTree.dataSource);
    }

    @Override
    public int hashCode() {
        int result = threadID.hashCode();
        result = 31 * result + dataSource.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return String.format("%s (%s)",this.dataSource,this.threadID);
    }

    public void printMetrics() {
        // TODO: could extract this with a single traversal
        System.out.println("Calltree " + this);
        System.out.println("\tsize: " + size());
        System.out.println("\tdepth: " + depth());
        System.out.println("\tFN: " + getFNs().size());
    }


}
