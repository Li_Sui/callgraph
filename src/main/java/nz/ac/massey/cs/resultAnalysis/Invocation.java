package nz.ac.massey.cs.resultAnalysis;

import com.google.common.base.Preconditions;

import java.util.*;

public class Invocation {

    private Method method = null;
    private String invocationId = null;
    private String threadName = null;
    private String tag = null;
    private boolean isFN = false;
    private int lineNumber = -1;
    private int depth = -1;
    private boolean isNative;
    private String threadId=null;
    private String threadObjectHash=null;


    private Invocation parent = null;
    private List<Invocation> children = new ArrayList<>(5);

    // hash used for structural
    private int structuralHash = -1;


    public Invocation(Method method, String invocationId, String threadName, String threadId, String threadObjectHash, String tag, boolean isFN, int lineNumber,int depth,boolean isNative) {
        Preconditions.checkArgument(method!=null);
        Preconditions.checkArgument(invocationId!=null);
        this.method = method;
        this.invocationId = invocationId;
        this.threadName = threadName;
        this.tag = tag;
        this.isFN = isFN;
        this.lineNumber = lineNumber;
        this.depth = depth;
        this.isNative=isNative;
        this.threadId=threadId;
        this.threadObjectHash=threadObjectHash;
    }

    public Method getMethod() {
        return method;
    }

    public String getInvocationId() {
        return invocationId;
    }

    public String getThreadName() {
        return threadName;
    }
    public String getThreadId(){return threadId;}

    public String getTag() {
        return tag;
    }
    public void setTag(String tag){this.tag=tag;}

    public boolean isFN() {
        return isFN;
    }
    public String getThreadObjectHash(){return threadObjectHash;}

    public int getLineNumber() {
        return lineNumber;
    }

    public boolean getIsNative(){return isNative;}

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Invocation that = (Invocation) o;

        if (!method.equals(that.method)) return false;
        return invocationId.equals(that.invocationId);
    }

    @Override
    public int hashCode() {
        int result = method.hashCode();
        result = 31 * result + invocationId.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return method.toString() + '@' + invocationId + (isFN?" FN":"") + (tag==null?" ":("#"+tag));
    }

    public int getDepth() {
        return depth;
    }


    public Invocation getParent() {
        return parent;
    }

    public Collection<Invocation> getChildren() {
        return children;
    }

    public boolean addChild(Invocation child) {
        child.parent = this;
        return children.add(child);
    }

    public boolean removeChild(Invocation child) {
        child.parent = null;
        return children.remove(child);
    }

    public void removeAllChildren() {
        this.children.clear();
    }

    public int getStructuralHash() {
        return structuralHash;
    }

    public void computeStructuralHash() {
        if (this.children.size()==0) {
            this.structuralHash = this.getMethod().hashCode();
        }
        else {
            Object[] components = new Object[children.size()+1];
            components[0] = this.method;
            for (int i=0;i<children.size();i++) {
                Invocation child = children.get(i);
                child.computeStructuralHash();
                components[i+1] = child.getStructuralHash();
            }
            this.structuralHash = Objects.hash(components);
        }
    }

    public boolean structuralSameAs(Invocation that) {
        if (this.children.size()==0) {
            return this.method.equals(that.method);
        }
        else {
            if (this.children.size()==that.children.size()) {
                for (int i=0;i<children.size();i++) {
                    if (!this.children.get(i).structuralSameAs(that.children.get(i))) {
                        return false;
                    }
                }
                return true;
            }
            else {
                return false;
            }
        }
    }
}
