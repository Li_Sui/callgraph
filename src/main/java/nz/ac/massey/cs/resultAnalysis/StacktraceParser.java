package nz.ac.massey.cs.resultAnalysis;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.TrueFileFilter;
import org.apache.commons.lang3.StringUtils;

import java.io.File;
import java.nio.charset.Charset;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * This class is used to verify edges that are generated dynamically.
 * Stacktraces are obtained via a script that uses the command "jstack" to take multiple thread dumpsMathematics Genealogy
 * @author Li Sui
 */
public class StacktraceParser {
    //location of where stacktraces are
    private static String stDir="stacktrace/stacktraces";
    private static String cgDir="stacktrace/cgoutput";
    private static String stack_element_regex = "at\\s((\\p{Alpha}|[0-9])\\.)*(\\p{Alnum}|\\_|\\$|\\>|\\<|\\.)+\\((\\p{Alnum}|\\.|\\:|\\p{Space}|\\[|\\])+\\)";
    private static String pre_signaler_package = "\\p{Alnum}(\\.|\\p{Alnum}|\\_|\\>|\\$|\\<|\\>|\\p{Space})+\\(";
    private static String cg_method_regex="\\+.+\\(";



    public static void main(String[] args) throws Exception{
        StacktraceParser parser=new StacktraceParser();
        List<Stacktrace> stacktraceList=parser.parseStacktraces();

        for(Stacktrace st: stacktraceList){

        }
    }

    private void parseCG() throws Exception{
        List<File> files = (List<File>) FileUtils.listFiles(new File(cgDir), TrueFileFilter.INSTANCE, TrueFileFilter.INSTANCE);
        for (File file : files) {
            String content = FileUtils.readFileToString(file, Charset.defaultCharset());
            String methodTable=content.split("-----")[0];
            String cg=content.split("-----")[1];
            Pattern pattern = Pattern.compile(cg_method_regex);
            Matcher matcher = pattern.matcher(methodTable);

            while (matcher.find()) {

                String result = StringUtils.substringBetween(matcher.group(), "+", " ").replace("{", ".");

            }
        }
    }

    private   List<Stacktrace> parseStacktraces() throws Exception{
        List<Stacktrace> sts=new ArrayList<>();
        List<File> files = (List<File>) FileUtils.listFiles( new File(stDir), TrueFileFilter.INSTANCE, TrueFileFilter.INSTANCE);
        for (File file : files) {
            String content=FileUtils.readFileToString(file, Charset.defaultCharset());
            //split content by empty line
            String[] threads=content.split("\\n\\n");

            for(String thread:threads){
                String threadName=StringUtils.substringBetween(thread,"\"","\"");
                String id=StringUtils.substringBetween(thread,"#"," ");

                if(id!=null){
                        Stacktrace st = new Stacktrace(threadName, Integer.valueOf(id));
                        Pattern pattern = Pattern.compile(stack_element_regex);
                        Matcher matcher = pattern.matcher(thread);
                        while (matcher.find()) {
                            String result = matcher.group();
                            String fullName = getName(result);
//                            String className=fullName.substring(0,fullName.lastIndexOf('.')-1);
//                            String methodName=fullName.substring(fullName.lastIndexOf('.')+1,fullName.length()-1);
                            st.addElement(fullName);
                        }
                        sts.add(st);
                }
            }
        }
        return sts;
    }



    private static String getName(String name) {
        Matcher matcher;
        int index;
        Pattern pattern = Pattern.compile(pre_signaler_package);
        String sig_met = null;
        if (name != null && !name.isEmpty() && (matcher = pattern.matcher(name)).find() && (index = (sig_met = matcher.group()).indexOf(40)) != -1) {
            sig_met = sig_met.substring(0, index);
            index = sig_met.lastIndexOf(32);
            if (index != -1) {
                sig_met = sig_met.substring(index, sig_met.length());
                sig_met = sig_met.trim();
            }
        }
        return sig_met;
    }

    public class Stacktrace{

        private String threadName;
        private int threadId;

        private List<String> stackElement=new ArrayList<>();
        public Stacktrace(String threadName,int threadId){

            this.threadName=threadName;
            this.threadId=threadId;
        }

        public void addElement(String element){
            stackElement.add(element);
        }

        public String getThreadName(){
            return this.threadName;
        }

        public int getThreadId(){
            return this.threadId;
        }
    }
}
