package nz.ac.massey.cs.resultAnalysis;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.tuple.Pair;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.nio.charset.Charset;
import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Stream;

/**
 * produce intermediate results for further cause analysis
 */
public class ProduceTagedFNs {
    //match exact java.lang.reflect.Method.invoke
    public static boolean isReflectionInvoke(Method method){
        return method.equals(Method.METHOD_INVOKE);
    }
    //match all java.lang.invoke.MethodHandle.invoke*
    public static boolean isMethodHandleInvoke(Method method){
        return method.getClassName().equals("java.lang.invoke.MethodHandle") && method.getName().startsWith("invoke");
    }
    //match invokedynamic
    public static boolean isInvokedynamic(Method method){
        return method.getName().contains("lambda$");
    }
    //match all InvocationHandler.invoke
    public static boolean isPorxyInvoke(Method method){
        return method.getDescriptor().equals("(Ljava/lang/Object;Ljava/lang/reflect/Method;[Ljava/lang/Object;)Ljava/lang/Object;") && method.getName().equals("invoke");
    }

    //match invocation
    public static boolean isInvocation(Method method){
        return isReflectionInvoke(method) || isMethodHandleInvoke(method) || isPorxyInvoke(method) || isInvokedynamic(method);
    }
    //match dynamic allocations
    public static boolean isAllocation(String tag){
        return isClassNewInstance(tag) || isConstructorNewInstance(tag) || isSerialisation(tag) || isUnsafe(tag) || isNativeReturn(tag);
    }
    public static boolean isClassNewInstance(String tag){
        return tag.equals("Class.newInstance");
    }
    public static boolean isConstructorNewInstance(String tag){
        return tag.equals("Constructor.newInstance");
    }
    public static boolean isSerialisation(String tag){
        return tag.equals("ObjectInputStream.readObject");
    }
    public static boolean isUnsafe(String tag){
        return tag.equals("Unsafe.getObject");
    }
    public static boolean isNativeReturn(String tag){
        return tag.equals("native.return");
    }

    //match Field.get
    public static boolean isDynamicAccess(String tag){
        return tag.equals("Field.get");
    }

    public static boolean isImplicit(String tag, String threadName){
        return matchNoSuchCallSite(tag) || matchSystemThread(threadName);
    }
    //match no such call site in a give list
    public static boolean matchNoSuchCallSite(String tag){
        return tag.equals("nocallsite");
    }
    //match already known system threads
    public static boolean matchSystemThread(String threadName){
        List<String> systemThread= Arrays.asList("AWT-EventQueue-0","AWT-Shutdown","Finalizer","DestroyJavaVM","Reference Handler","Signal Dispatcher");
        return systemThread.contains(threadName);
    }

    public static boolean matchAllCategories(Method method,String tag,String threadName){
        return isImplicit(tag,threadName) || isDynamicAccess(tag) || isAllocation(tag) || isInvocation(method);
    }
    //static String OUTPUTHEADER="program, reflection invoke, handler invoke, invokedynamic, proxy invoke, classNewInstance, constructorNewInstance ,serialisation, unsafe, native return,field get,nosuchcallsite,system thread\n";
    public static void main(String[] args) throws Exception{
        Options options = new Options()
                .addOption("inputDir", true, "processed-CCTs-data folder containing all dataset, including programs, CCTs,FNs and invocation list");
        CommandLine cmd = new DefaultParser().parse(options, args);

        if ( !cmd.hasOption("inputDir")) {
            HelpFormatter formatter = new HelpFormatter();
            formatter.printHelp("java -cp <classpath> <options> " + RQ3Spreadsheet.class.getName(), options);
            System.exit(0);
        }

        String inputDir=cmd.getOptionValue("inputDir");


        for(File f: new File(inputDir).listFiles()) {
            if(f.isDirectory()) {
                String program = f.getName();

                String analysis = "contextInsensitive";
                System.out.println("processing " + program + ", base");
                processFNs(inputDir + "/" + program, analysis);
                analysis = "reflection";
                System.out.println("processing " + program + ", reflection");
                processFNs(inputDir + "/" + program, analysis);
            }
        }
    }

    public static synchronized void processFNs(String programDir,String analysis) throws Exception{
        String fn_bt=programDir+"/FNs-builtinTest/FNs-"+analysis+"-lib.csv";
        String fn_gt=programDir+"/FNs-generatedTest/FNs-"+analysis+"-lib.csv";

        String recordings_bt=programDir+"/CCTs-builtinTest";
        String recordings_gt=programDir+"/CCTs-generatedTest";
        StringBuilder sb =new StringBuilder();
        //category: invocation
        Set<Method> combined_reflection_invoke=new HashSet<>();
        combined_reflection_invoke.addAll(getReflectionInvokeFNs(fn_bt,recordings_bt));combined_reflection_invoke.addAll(getReflectionInvokeFNs(fn_gt,recordings_gt));
        toString(combined_reflection_invoke,"method.invoke",sb);
        Set<Method> combined_handler_invoke=new HashSet<>();
        combined_handler_invoke.addAll(getMethodHandleInvokeFNs(fn_bt,recordings_bt));combined_handler_invoke.addAll(getMethodHandleInvokeFNs(fn_gt,recordings_gt));
        toString(combined_handler_invoke,"handler.invoke",sb);
        Set<Method> combined_invokedynamic=new HashSet<>();
        combined_invokedynamic.addAll(getInvokedynamicFNs(fn_bt,recordings_bt));combined_invokedynamic.addAll(getInvokedynamicFNs(fn_gt,recordings_gt));
        toString(combined_invokedynamic,"lambda",sb);
        Set<Method> combined_proxy_invoke=new HashSet<>();
        combined_proxy_invoke.addAll(getPorxyInvokeFNs(fn_bt,recordings_bt)); combined_proxy_invoke.addAll(getPorxyInvokeFNs(fn_gt,recordings_gt));
        toString(combined_proxy_invoke,"dynproxy.invoke",sb);
        //category: allocation
        Set<Method> combined_classNewInstance=new HashSet<>();
        combined_classNewInstance.addAll(getClassNewInstanceFNs(fn_bt,recordings_bt));combined_classNewInstance.addAll(getClassNewInstanceFNs(fn_gt,recordings_gt));
        toString(combined_classNewInstance,"class.newinstance",sb);
        Set<Method> combined_constructorNewInstance=new HashSet<>();
        combined_constructorNewInstance.addAll(getConstructorNewInstanceFNs(fn_bt,recordings_bt));combined_constructorNewInstance.addAll(getConstructorNewInstanceFNs(fn_gt,recordings_gt));
        toString(combined_constructorNewInstance,"constr.newinstance",sb);
        Set<Method> combined_serialisation=new HashSet<>();
        combined_serialisation.addAll(getSerialisationFNs(fn_bt,recordings_bt));combined_serialisation.addAll(getSerialisationFNs(fn_gt,recordings_gt));
        toString(combined_serialisation,"deserialize",sb);
        Set<Method> combined_unsafe=new HashSet<>();
        combined_unsafe.addAll(getUnsafeFNs(fn_bt,recordings_bt));combined_unsafe.addAll(getUnsafeFNs(fn_gt,recordings_gt));
        toString(combined_unsafe,"unsafe.getobject",sb);
        Set<Method> combined_nativereturn=new HashSet<>();
        combined_nativereturn.addAll(getNativeReturnFNs(fn_bt,recordings_bt));combined_nativereturn.addAll(getNativeReturnFNs(fn_gt,recordings_gt));
        toString(combined_nativereturn,"nativeallocation",sb);
        //category: dynamic access
        Set<Method> combined_fieldGet=new HashSet<>();
        combined_fieldGet.addAll(getDynAccessFNs(fn_bt,recordings_bt));combined_fieldGet.addAll(getDynAccessFNs(fn_gt,recordings_gt));
        toString(combined_fieldGet,"field.get",sb);
        //category: implicit
        Set<Method> combined_nosuchcallsite=new HashSet<>();
        combined_nosuchcallsite.addAll(getNoSuchCallsiteFNs(fn_bt,recordings_bt));combined_nosuchcallsite.addAll(getNoSuchCallsiteFNs(fn_gt,recordings_gt));
        toString(combined_nosuchcallsite,"nocallsite",sb);
        Set<Method> combined_systemThread=new HashSet<>();
        combined_systemThread.addAll(getSystemTheadFNs(fn_bt,recordings_bt));combined_systemThread.addAll(getSystemTheadFNs(fn_gt,recordings_gt));
        toString(combined_systemThread,"systemthread",sb);


        Set<Method> combined_allTagged=new HashSet<>();
        combined_allTagged.addAll(getAlltaggedFNs(fn_bt,recordings_bt));combined_allTagged.addAll(getAlltaggedFNs(fn_gt,recordings_gt));
        toString(combined_allTagged,"other",sb);

        String result=sb.toString().replaceAll("(?m)^[ \t]*\r?\n", "");
        IOUtils.write(result,new FileOutputStream(new File(programDir+"/FNS-tagged-lib-combined-"+analysis+".csv")), Charset.defaultCharset());
    }

    private static void toString(Set<Method> methodSet,String tag,StringBuilder sb){
        for(Method m: methodSet){
            sb.append(buildMethod(m)+","+tag);
            sb.append("\n");
        }
    }

    public static Set<Method> getAlltaggedFNs(String fnFile,String recordings) throws Exception{
        String[] arg= {"-fn",fnFile,"-recordings",recordings};
        Predicate<Invocation> filter = inv -> !matchAllCategories(inv.getMethod(),inv.getTag(),inv.getThreadName());
        return getReminingFNs(filter ,arg);
    }

    public static Set<Method> getClassNewInstanceFNs(String fnFile,String recordings) throws Exception{
        String[] arg= {"-fn",fnFile,"-recordings",recordings};
        Predicate<Invocation> filter = inv -> !isClassNewInstance(inv.getTag());
        return getRemovedFNs(filter ,arg);
    }


    public static Set<Method> getConstructorNewInstanceFNs(String fnFile,String recordings) throws Exception{
        String[] arg= {"-fn",fnFile,"-recordings",recordings};
        Predicate<Invocation> filter = inv -> !isConstructorNewInstance(inv.getTag());
        return getRemovedFNs(filter ,arg);
    }

    public static Set<Method> getSerialisationFNs(String fnFile,String recordings) throws Exception{
        String[] arg= {"-fn",fnFile,"-recordings",recordings};
        Predicate<Invocation> filter = inv -> !isSerialisation(inv.getTag());
        return getRemovedFNs(filter ,arg);
    }

    public static Set<Method> getUnsafeFNs(String fnFile,String recordings) throws Exception{
        String[] arg= {"-fn",fnFile,"-recordings",recordings};
        Predicate<Invocation> filter = inv -> !isUnsafe(inv.getTag());
        return getRemovedFNs(filter ,arg);
    }


    public static Set<Method> getNativeReturnFNs(String fnFile,String recordings) throws Exception{
        String[] arg= {"-fn",fnFile,"-recordings",recordings};
        Predicate<Invocation> filter = inv -> !isNativeReturn(inv.getTag());
        return getRemovedFNs(filter ,arg);
    }


    public static Set<Method> getReflectionInvokeFNs(String fnFile,String recordings) throws Exception{
        String[] arg= {"-fn",fnFile,"-recordings",recordings};
        Predicate<Invocation> filter = inv -> !isReflectionInvoke(inv.getMethod());
        return getRemovedFNs(filter ,arg);
    }

    public static Set<Method> getMethodHandleInvokeFNs(String fnFile,String recordings) throws Exception{
        String[] arg= {"-fn",fnFile,"-recordings",recordings};
        Predicate<Invocation> filter = inv -> !isMethodHandleInvoke(inv.getMethod());
        return getRemovedFNs(filter ,arg);
    }

    public static Set<Method> getInvokedynamicFNs(String fnFile,String recordings) throws Exception{
        String[] arg= {"-fn",fnFile,"-recordings",recordings};
        Predicate<Invocation> filter = inv -> !isInvokedynamic(inv.getMethod());
        return getRemovedFNs(filter ,arg);
    }

    public static Set<Method> getPorxyInvokeFNs(String fnFile,String recordings) throws Exception{
        String[] arg= {"-fn",fnFile,"-recordings",recordings};
        Predicate<Invocation> filter = inv -> !isPorxyInvoke(inv.getMethod());
        return getRemovedFNs(filter ,arg);
    }



    public static Set<Method> getDynAccessFNs(String fnFile,String recordings) throws Exception{

        //remove branches that caused by dynamic access
        Predicate<Invocation> dynamicAccess = inv -> !isDynamicAccess(inv.getTag());
        String[] arg= {"-fn",fnFile,"-recordings",recordings};

        return getRemovedFNs(dynamicAccess,arg);
    }
    public static Set<Method> getSystemTheadFNs(String fnFile,String recordings) throws Exception{
        String[] arg= {"-fn",fnFile,"-recordings",recordings};
        //remove branches that caused by invocation without such callsite
        Predicate<Invocation> filter = inv -> !matchSystemThread(inv.getThreadName());

        return getRemovedFNs(filter,arg);
    }

    public static Set<Method> getNoSuchCallsiteFNs(String fnFile,String recordings) throws Exception{
        String[] arg= {"-fn",fnFile,"-recordings",recordings};

        //remove branches that caused by invocation without such callsite
        Predicate<Invocation> filter = inv -> !matchNoSuchCallSite(inv.getTag());

        return getRemovedFNs(filter,arg);
    }


    public static Set<Method> getReminingFNs(Predicate<Invocation> filter, String[] arg) throws Exception{
        Pair<Set<Method>, Stream<CallTree>> data = IO.importDataForProcessing(arg);
        Set<Method> fnsDetectedAfterFiltering = Collections.synchronizedSet(new HashSet<>());
        data.getRight()
                .forEach(
                        callTree -> {
                            callTree.dfsParallel(filter, inv -> {
                                if (inv.isFN()) {
                                    fnsDetectedAfterFiltering.add(inv.getMethod());
                                }
                            });
                        }
                );

        return fnsDetectedAfterFiltering;
    }

    public static Set<Method> getRemovedFNs(Predicate<Invocation> filter, String[] arg) throws Exception{
        Pair<Set<Method>, Stream<CallTree>> data = IO.importDataForProcessing(arg);
        Set<Method> fnsDetectedAfterFiltering = Collections.synchronizedSet(new HashSet<>());
        Set<Method> fns=data.getLeft();
        data.getRight()
                .forEach(
                        callTree -> {
                            callTree.dfsParallel(filter, inv -> {
                                if (inv.isFN()) {
                                    fnsDetectedAfterFiltering.add(inv.getMethod());
                                }
                            });
                        }
                );
        fns.removeAll(fnsDetectedAfterFiltering);
        return fns;
    }

    public static String buildMethod(Method m){
        return m.getClassName()+","+m.getName()+","+m.getDescriptor();
    }
}

