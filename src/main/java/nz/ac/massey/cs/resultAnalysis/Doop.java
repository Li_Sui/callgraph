package nz.ac.massey.cs.resultAnalysis;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.HashSet;
import java.util.Set;

public class Doop {

    private String setup;
    private String root;


    public Doop(String setup,String root){
        this.setup=setup;
        this.root=root;
    }

    public Set<String> getDoopBase_set(){
        File doop_base_file=new File(root+"/base/Reachable.csv");
        return (doop_base_file.exists())?constructDoopReachableSet(doop_base_file):new HashSet<>();
    }

    public Set<String> getDoopCallsite_set(){
        File doop_callsite_file=new File(root+"/callsite/Reachable.csv");
        return (doop_callsite_file.exists())?constructDoopReachableSet(doop_callsite_file):new HashSet<>();
    }

    public Set<String> getDoopReflection_set(){
        File doop_reflection_file=new File(root+"/reflection/Reachable.csv");
        return (doop_reflection_file.exists())?constructDoopReachableSet(doop_reflection_file) :new HashSet<>();
    }

    public Set<String> getDoopReflectionLite_set(){
        File doop_reflectionLite_file=new File(root+"/reflection-lite/Reachable.csv");
        return (doop_reflectionLite_file.exists())?constructDoopReachableSet(doop_reflectionLite_file):new HashSet<>();
    }

    public String getSetup(){
        return this.setup;
    }


    private static Set<String> constructDoopReachableSet(final File doopFile){
        Set<String> reachable = new HashSet<>();
        try {
            String[] reachableSet = FileUtils.readFileToString(doopFile, Charset.defaultCharset()).split(System.getProperty("line.separator"));
            for (String method : reachableSet) {
                reachable.add(convertDoopMethod(method));
            }
        }catch(IOException e){
            e.printStackTrace();
        }
        return reachable;
    }

    /**
     * convert Doop methods
     * @param method
     * @return converted string
     */
    public static String convertDoopMethod(String method){

        String className= StringUtils.substringBetween(method,"<",":");

        String returnType= parseString(StringUtils.substringBetween(method,": ","(").split("\\s")[0]);
        String methodName= StringUtils.substringBetween(method,": ","(").split("\\s")[1];
        String doopParameters=StringUtils.substringBetween(method,"(",")");
        String parameters="";
        if(doopParameters.contains(",")) {
            for (String p : doopParameters.split(",")){
                parameters=parameters+parseString(p);
            }
        }else{
            parameters=parseString(doopParameters);
        }
        if(doopParameters.equals("")){
            parameters="";
        }

        return className+","+methodName+",("+parameters+")"+returnType;
    }

    private static String parseString(String s){
        String result="";

        if(s.equals("void")) {
            return "V";
        }
        if(isPrimitive(s)) {
            if (s.startsWith("int")) {
                result = "I";
            }

            if (s.startsWith("byte")) {
                result = "B";
            }
            if (s.startsWith("long")) {
                result = "J";
            }

            if (s.startsWith("float")) {
                result = "F";
            }

            if (s.startsWith("double")) {
                result = "D";
            }
            if (s.startsWith("short")) {
                result = "S";
            }

            if (s.startsWith("char")) {
                result = "C";
            }

            if (s.startsWith("boolean")) {
                result = "Z";
            }

            if(s.contains("[]")){
                int count=StringUtils.countMatches(s,"[");
                String tmp="";
                for(int i=0;i<count;i++){
                    tmp=tmp+"[";
                }
                result=tmp+result;
            }
            return  result;
        }else {
            result="L"+s.replaceAll("\\.","/").replaceAll("\\[\\]","")+";";

            if (s.contains("[]")) {
                int count = StringUtils.countMatches(s, "[");
                String tmp = "";
                for (int i = 0; i < count; i++) {
                    tmp = tmp + "[";
                }
                result = tmp + result;
            }


            return result;
        }
    }

    private static boolean isPrimitive(String s){

        if(s.startsWith("int") || s.startsWith("byte") || s.startsWith("long") || s.startsWith("float")
                || s.startsWith("double") || s.startsWith("short") || s.startsWith("char") ||s.startsWith("boolean")){
            return true;
        }else{
            return false;
        }
    }
}
