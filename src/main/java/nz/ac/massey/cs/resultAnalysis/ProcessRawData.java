package nz.ac.massey.cs.resultAnalysis;

import org.apache.commons.io.FileUtils;

import java.io.*;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

/**
 * Process raw stack data. decode hashcode to string. assign each invocation with an ID
 *
 *  input data format:
 *  stack.csv:    hashCode,kind,tag,threadID,threadObjectHash\tthreadName\tdepth
 *  stringMap.csv hashcode\tclassLoader,className,methodName,descriptor
 *
 *  output data format:
 *  classLoader,className,methodName,descriptor,kind,tag,threadID,threadObjectHash,methodID\tthreadName\tdepth
 *
 *  when running on larger datasets, grant  more heap and stack memory, e.g.  -Xmx60g -Xss512m
 */
public class ProcessRawData {
    //static  Method instrumentation2=Method.getInstance("","sun.instrument.TransformerManager","transform","(Ljava/lang/ClassLoader;Ljava/lang/String;Ljava/lang/Class;Ljava/security/ProtectionDomain;[B)[B");

    public static void main (String[] args) throws Exception {

        if(args.length==0){
            System.err.println("args[0]=path to raw ccts, args[1]=output dir");
            return;
        }

        File recordingsFolder = new File(args[0]);
        String outputDir=args[1];


        //before processing raw data, check CCTS folder for already completed CCTs
        List<String> finishedCCTs=new ArrayList<>();
        File outputFolder=new File(outputDir);
        Iterator<File> fileIterator= FileUtils.iterateFiles(outputFolder,null,true);
        while(fileIterator.hasNext()){
            File finishedCCTFile=fileIterator.next();
            if(finishedCCTFile.getName().contains("Driver_")) {
                finishedCCTs.add(finishedCCTFile.getName().split("\\.")[0] + ".zip");
            }
        }

        FileFilter filter = f -> f.getName().endsWith(".zip");

        for (File stackData : recordingsFolder.listFiles(filter)) {
            if(!finishedCCTs.contains(stackData.getName())) {
                try {
                    System.out.println("extracting " + stackData.getName());
                    InputStream stackInputStream = getInputStream(stackData, "stack.csv");
                    InputStream stringMapInputStream = getInputStream(stackData, "stringMap.csv");
                    String fileName = stackData.getName().split("\\.")[0] + ".csv";
                    Map<Integer, String> stringMap = getStringMap(stringMapInputStream);
                    //decode stack
                    //long t1 = System.currentTimeMillis();
                    System.out.println("-building CCT...");
                    Map<String, CallTree> callTreeMap = buildCallTree(outputDir, stackInputStream, stringMap, fileName);
                    //long t2 = System.currentTimeMillis();
                    //System.out.println("\tthis took: " + (t2-t1) + "ms");
                    // t1 = System.currentTimeMillis();
                    System.out.println("-removing branches...");
                    for (String threadID : callTreeMap.keySet()) {
                        CallTree callTree = callTreeMap.get(threadID);
                        CallTreeReducer.removeRedundantBranches(callTree);
                        CallTreeReducer.removeInstrumentationBranches(callTree, Method.INSTRUMENTATION);
                        IO.recreateStackFileAfterReduction(outputDir, callTree);
                    }
                    //  t2 = System.currentTimeMillis();
                    // System.out.println("\tthis took: " + (t2-t1) + "ms");

                    stackInputStream.close();
                    stringMapInputStream.close();
                }catch (OutOfMemoryError e){
                    System.err.println("[error]outofmemory:"+stackData.getName()+"##");
                }
            }
        }
    }

    private static AtomicInteger counter = new AtomicInteger();

    private static InputStream getInputStream(File zip, String entry) throws IOException {
        ZipInputStream zin = new ZipInputStream(new FileInputStream(zip));
        for (ZipEntry e; (e = zin.getNextEntry()) != null;) {
            if (e.getName().equals(entry)) {
                return zin;
            }
        }
        throw new EOFException("Cannot find " + entry);
    }
    public static Map<String,CallTree> buildCallTree(String outputDir,InputStream stream,Map<Integer,String> stringMap,String fileName) throws Exception{
        BufferedReader reader = new BufferedReader(new InputStreamReader(stream));
        Map<String, Stack<Invocation>> stacks = new HashMap<>();
        Map<String, CallTree> callTrees = new HashMap<>();
        Set<String> invocationIDset=new HashSet<>();
        int lineNumber=0;
        String line = null;
        while ((line = reader.readLine()) != null) {
            lineNumber++;
            //UUID uuid = UUID.randomUUID();

            String[] elements=line.split("\t");
            assert elements.length==3; //hashCode,kind,tag,threadID,threadObjectHash\tthreadName\tdepth

            String[] methodToken=elements[0].split(",");
            assert methodToken.length==6;//classLoader,className,methodName,descriptor,invocationID

            String classloaderName=stringMap.get(Integer.valueOf(methodToken[0])).split(",")[0];
            String className=stringMap.get(Integer.valueOf(methodToken[0])).split(",")[1];
            String methodName=stringMap.get(Integer.valueOf(methodToken[0])).split(",")[2];
            String descriptor=stringMap.get(Integer.valueOf(methodToken[0])).split(",")[3];
            String invocationID=methodToken[1];

            if(invocationIDset.contains(invocationID)){
                System.err.println("[error]file: "+fileName+", line "+lineNumber +", has duplicate invocation ID");
            }else{
                invocationIDset.add(invocationID);
            }
            String kind= (Integer.valueOf(methodToken[2])==0) ? "none-native" : "native";
            boolean isNative = kind.equals("native");
            String tag=getTag(Integer.valueOf(methodToken[3]));
            String threadID=methodToken[4];
            String threadObjectHashcode=methodToken[5];

            String threadName=elements[1];
            int depth=Integer.valueOf(elements[2]);

            if(kind.equals("native")){
                if(!tag.equals("noTag")) {
                    System.err.println("[error]a native method cannot have any tags at file: "+fileName+", line "+lineNumber);
                    System.exit(1);
                }
            }
            Method method = Method.getInstance(classloaderName, className, methodName,descriptor);
            if(method.equals(Method.THREAD_EXIT)){
                assert depth==1;
            }
            CallTree callTree = callTrees.computeIfAbsent(threadID, t -> new CallTree(threadID, fileName));
            Stack<Invocation> stack = stacks.computeIfAbsent(threadID, t -> new Stack<Invocation>());

            Invocation invocation = new Invocation(method, invocationID, threadName,threadID,threadObjectHashcode, tag, false, lineNumber, depth, isNative);
            Invocation rootInv=new Invocation(Method.ROOT_METHOD,"-1",threadName,threadID,threadObjectHashcode,"null",false,-1,0,false);

            //set root
            if (!callTree.hasRoot()) {
                assert invocation.getDepth() == 1;
                callTree.setRoot(rootInv);
            }
            //if depth is 1, added to root
            if(depth==1){
                callTree.getRoot().addChild(invocation);
            }

            while (depth <= stack.size()) {
                stack.pop();
            }
            Invocation callsite = stack.isEmpty()?null:stack.peek();
            stack.push(invocation);
            assert stack.size()==invocation.getDepth();
            if (callsite!=null) {
                assert callsite.getDepth()+1 == invocation.getDepth();
                callTree.addEdge(callsite, invocation);
            }
        }
        reader.close();
        return callTrees;
    }

    public static Map<Integer,String> getStringMap(InputStream stream) throws Exception{
        Map<Integer,String> stringMap=new HashMap<>();
        BufferedReader reader = new BufferedReader(new InputStreamReader(stream));
        String line = null;
        while ((line = reader.readLine()) != null) {
            String[] elements=line.split("\t");
            stringMap.put(Integer.valueOf(elements[0]),elements[1]);
        }
        reader.close();
        return stringMap;
    }

    private static String getTag(int i){
        if(i==0){
            return "Class.newInstance";
        }
        if(i==1){
            return "Constructor.newInstance";
        }
        if(i==2){
            return "ObjectInputStream.readObject";
        }
        if(i==3){
            return "Field.get";
        }
        if(i==4){
            return "Unsafe.getObject";
        }
        if(i==5){
            return "native.return";
        }
        return "noTag";
    }
}