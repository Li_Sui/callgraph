package nz.ac.massey.cs.resultAnalysis;

import com.google.common.base.Preconditions;

public class PivotalInvocation {

    private CallTree callTree = null;
    private Invocation FN = null;
    private Invocation callsite = null;

    public static PivotalInvocation NIL = new PivotalInvocation();

    private PivotalInvocation(){}

    public PivotalInvocation(CallTree callTree, Invocation FN, Invocation callsite) {
        this.callTree = callTree;
        this.FN = FN;
        this.callsite = callsite;
        Preconditions.checkArgument(FN.isFN());
    }

    public CallTree getCallTree() {
        return callTree;
    }

    public Invocation getFN() {
        return FN;
    }

    public Invocation getCallsite() {
        return callsite;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PivotalInvocation that = (PivotalInvocation) o;

        if (!callTree.equals(that.callTree)) return false;
        if (!FN.equals(that.FN)) return false;
        return callsite.equals(that.callsite);
    }

    @Override
    public int hashCode() {
        int result = callTree.hashCode();
        result = 31 * result + FN.hashCode();
        result = 31 * result + callsite.hashCode();
        return result;
    }
}
