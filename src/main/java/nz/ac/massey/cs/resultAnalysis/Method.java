package nz.ac.massey.cs.resultAnalysis;


import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Instances are controlled by a static registry to reuse them and save memory.
 * Having only string properties, instances are immutable and can be easily shared.
 * @author jens dietrich
 */
public class Method {

    private static Map<String,Method> instances = new ConcurrentHashMap<>();
    private String classLoader=null;
    private String className = null;
    private String name = null;
    private String descriptor = null;

    public static Method ROOT_METHOD=Method.getInstance("null","null","null","null");
    public static Method THREAD_EXIT=Method.getInstance("systemLoader","java.lang.Thread","exit","()V");
    public static Method METHOD_INVOKE = Method.getInstance("systemLoader","java.lang.reflect.Method","invoke","(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;");
    public static Method LOAD_CLASS = Method.getInstance("systemLoader","java.lang.ClassLoader","loadClass","(Ljava/lang/String;)Ljava/lang/Class;");
    public static Method SHUTDOWN_SHUTDOWN = Method.getInstance("systemLoader","java.lang.Shutdown","shutdown","()V");
    public static Method FINALIZER_ACCESS = Method.getInstance("systemLoader","java.lang.ref.Finalizer","access$100","(Ljava/lang/ref/Finalizer;Lsun/misc/JavaLangAccess;)V");
    public static Method INSTRUMENTATION = Method.getInstance("systemLoader","sun.instrument.InstrumentationImpl","transform","(Ljava/lang/ClassLoader;Ljava/lang/String;Ljava/lang/Class;Ljava/security/ProtectionDomain;[BZ)[B");

    private Method(String classLoader,String className, String name, String descriptor) {
        this.classLoader=classLoader;
        this.className = className;
        this.name = name;
        this.descriptor = descriptor;
    }


    public static Method getInstance(String classLoader,String className, String name, String descriptor) {
        //String id = String.format("%s#%s#%s", classLoader,className,name,descriptor);
        String id = classLoader + "#" + className + "#" + name + "#" + descriptor;
        return instances.computeIfAbsent(id,k -> new Method(classLoader,className, name, descriptor));
    }
    public String getClassLoader(){return classLoader;}

    public String getClassName() {
        return className;
    }

    public String getName() {
        return name;
    }

    public String getDescriptor() {
        return descriptor;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Method method = (Method) o;

        if (!className.equals(method.className)) return false;
        if (!name.equals(method.name)) return false;
        return descriptor.equals(method.descriptor);
    }

    @Override
    public int hashCode() {
        int result = className.hashCode();
        result = 31 * result + name.hashCode();
        result = 31 * result + descriptor.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return className + '#' + name  + '-' + descriptor;
    }
}
