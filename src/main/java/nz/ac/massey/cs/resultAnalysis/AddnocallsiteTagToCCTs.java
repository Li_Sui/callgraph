package nz.ac.massey.cs.resultAnalysis;



import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.io.FileUtils;


import java.io.*;
import java.nio.charset.Charset;
import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;


public class AddnocallsiteTagToCCTs {



    public static void main(String[] args) throws Exception{
        Options options = new Options()
                .addOption("programDir", true, " recordings,FNs and invocation list")
                .addOption("outputDir",true,"output folder");
        CommandLine cmd = new DefaultParser().parse(options, args);

        if (!cmd.hasOption("outputDir")|| !cmd.hasOption("programDir")) {
            HelpFormatter formatter = new HelpFormatter();
            formatter.printHelp("java -cp <classpath> <options> " + AddnocallsiteTagToCCTs.class.getName(), options);
            System.exit(0);
        }

        String programDir=cmd.getOptionValue("programDir");
        String outputDir=cmd.getOptionValue("outputDir");
        File jdkInvocationListFile=new File(programDir+"/jdk1.8.0_144-invocationList.csv");

        File outputFolder1 = new File(outputDir +"/CCTs-builtinTest");
        File outputFolder2 = new File(outputDir + "/CCTs-generatedTest");
        if (!outputFolder1.exists() || !outputFolder2.exists() ) {
            outputFolder1.mkdirs();
            outputFolder2.mkdirs();
        }


        Map<Method, List<Callsite>> callsiteMap=loadCallsiteList(programDir,jdkInvocationListFile);

        processFNs(programDir,callsiteMap,outputDir);

    }

    public static void processFNs(String programDir,Map<Method, List<Callsite>> callsiteMap,String outputDir) throws Exception{


        tagNocallsite(programDir+"/FNs-builtinTest",programDir+"/CCTs-builtinTest",callsiteMap,outputDir);
        tagNocallsite(programDir+"/FNs-generatedTest",programDir+"/CCTs-generatedTest",callsiteMap,outputDir);

    }

    public static synchronized void tagNocallsite(String fnRoot,String recordings, final Map<Method, List<Callsite>> callsiteMap,final String outputDir) throws Exception{
        File fn_base=new File(fnRoot+"/FNs-contextInsensitive-lib.csv");
        File fn_callsite=new File(fnRoot+"/FNs-contextSensitive-lib.csv");
        File fn_reflection=new File(fnRoot+"/FNs-reflection-lib.csv");
        File fn_reflectionLite=new File(fnRoot+"/FNs-reflectionLite-lib.csv");

        Set<Method> fns=new HashSet<>();
        fns.addAll(IO.readFNs(fn_base));fns.addAll(IO.readFNs(fn_callsite));fns.addAll(IO.readFNs(fn_reflection));fns.addAll(IO.readFNs(fn_reflectionLite));

        FileFilter filter = f -> f.getName().endsWith(".csv");
        for (File f: new File(recordings).listFiles(filter)){

                Map<String, CallTree> callTreeMap = IO.buildCallTrees(f, fns);

                for (CallTree callTree : callTreeMap.values()) {
                    analyse(callTree, callsiteMap);
                    writeToFile(outputDir, callTree);
                }

        }

    }



    private static synchronized void analyse(CallTree callTree,final Map<Method, List<Callsite>> callsiteMap) {
        // select FNs
        Set<Invocation> fns = callTree.getInvocations().parallelStream()
                .filter(inv -> inv.isFN())
                .collect(Collectors.toSet());


        fns.parallelStream().forEach(fn-> {
            for(Invocation inv:callTree.findPathToRootFrom(fn)){
                Invocation parent=inv.getParent();
                if(parent!=null){
                    if(!parent.getMethod().getClassName().equals("null")) {
                        if (!findSuchCallsite(callsiteMap, inv.getMethod(), parent.getMethod())) {
                            if (inv.getTag().equals("noTag")) {
                                inv.setTag("nocallsite");
                            }
                        }
                    }
                }
            }
        });
    }
    private static Map<Method, List<Callsite>> loadCallsiteList(String path, File jdkInvocationListFile) throws Exception{
        File programInvocationListFile = new File(path+ "/program-invocationList.csv");
        File libInvocationListFile = new File(path+"/library-invocationList.csv");
        //load callsite list
        String[] jdkInvocationList = FileUtils.readFileToString(jdkInvocationListFile, Charset.defaultCharset()).split(System.getProperty("line.separator"));
        String[] programInvocationList = FileUtils.readFileToString(programInvocationListFile, Charset.defaultCharset()).split(System.getProperty("line.separator"));
        String[] libInvocationList = FileUtils.readFileToString(libInvocationListFile, Charset.defaultCharset()).split(System.getProperty("line.separator"));
        Map<Method, List<Callsite>> callsiteMap = new HashMap<>();
        buildCallsiteMap(jdkInvocationList, callsiteMap);
        buildCallsiteMap(programInvocationList, callsiteMap);
        if (libInvocationList.length > 1) {
            buildCallsiteMap(libInvocationList, callsiteMap);
        }
        return callsiteMap;
    }

    public static boolean findSuchCallsite(Map<Method, List<Callsite>> callsiteMap, Method child, Method parent) {
        boolean found = false;
        if (callsiteMap.containsKey(parent)) {
            for (Callsite callsite : callsiteMap.get(parent)) {
                if (callsite.getName().equals(child.getName()) && callsite.getDescriptor().equals(child.getDescriptor())) {
                    found = true;
                    break;
                }
            }
        }
        return found;
    }

    public static void buildCallsiteMap(String[] invocationList, Map<Method, List<Callsite>> callsiteMap) {

        for (String invocation : invocationList) {
            String method = invocation.split("\t")[0];
            Method m = Method.getInstance("", method.split("\\,")[0], method.split("\\,")[1], method.split("\\,")[2]);
            List<Callsite> callsiteList = new ArrayList<>();
            if (invocation.split("\t").length > 1) {
                for (String callsite : invocation.split("\t")[1].split("\\#")) {
                    String name = callsite.split("\\,")[0];
                    String descriptor = callsite.split("\\,")[1];
                    callsiteList.add(new Callsite(name, descriptor));
                }
            }
            callsiteMap.put(m, callsiteList);
        }
    }

    public static void writeToFile(String outputDir, CallTree callTree) throws IOException {
        Predicate<Invocation> filter = inv -> true;
        String name=callTree.getDataSource();
        if(name.startsWith("Driver_builtinTest")){
            outputDir=outputDir+"/CCTs-builtinTest";
        }
        if(name.startsWith("Driver_generatedTest")){
            outputDir=outputDir+"/CCTs-generatedTest";
        }
        FileWriter fw = new FileWriter(outputDir+'/'+callTree.getDataSource(),true);
        BufferedWriter bw = new BufferedWriter(fw);
        callTree.dfs(filter, inv -> {
            if(inv.getDepth()!=0) {
                Method method = inv.getMethod();

                String kind = inv.getIsNative() ? "native" : "none-native";
                try {
                    // bw.write(method.getClassLoader()+","+method.getClassName() + "," + method.getName() + "," + method.getDescriptor() + "," + kind + "," + inv.getTag() + "," + inv.getThreadId()+","+inv.getThreadObjectHash()+","+inv.getInvocationId() + "\t" + inv.getThreadName() + "\t" + inv.getDepth()+"\n");
                    bw.write(method.getClassLoader());
                    bw.write(",");
                    bw.write(method.getClassName());
                    bw.write(",");
                    bw.write(method.getName());
                    bw.write(",");
                    bw.write(method.getDescriptor());
                    bw.write(",");
                    bw.write(kind);
                    bw.write(",");
                    bw.write(inv.getTag());
                    bw.write(",");
                    bw.write(inv.getThreadId());
                    bw.write(",");
                    bw.write(inv.getThreadObjectHash());
                    bw.write(",");
                    bw.write(inv.getInvocationId());
                    bw.write("\t");
                    bw.write(inv.getThreadName());
                    bw.write("\t");
                    bw.write(String.valueOf(inv.getDepth()));
                    bw.write("\n");
                } catch (IOException e) {
                }
            }
        });
        bw.close();
        fw.close();

    }
}
