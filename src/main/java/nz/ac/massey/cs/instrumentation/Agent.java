

package nz.ac.massey.cs.instrumentation;

import java.io.*;
import java.lang.instrument.Instrumentation;
import java.util.ArrayList;
import java.util.List;


/**
 * input: a collection of native callsite
 * StaticInstrumenter
 * packageBlackList // filter out classes/packages
 * shutdownHook // write all edges to a file
 *t.
 * push(threadname,classloadername+callname,threadId)) // push a method onto the stack
 * pop(threadId) //pop a method from the stack
 * clear(classloadername+callname,threadId) //used for dealing with Exception
 *
 * @author li sui
 */
public class Agent {

    public static List<String> nativeCallsite=new ArrayList<>();


    public static void premain(String args, Instrumentation instrumentation)  throws Exception{
        if (args == null) {
            return;
        }
        //load the native logger
        System.load(new File("nativelogger.so").getAbsolutePath());

        //produce file
        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                NativeLogger.flush();
            }
        });


        String recordedNativeFile=args;
        //load recorded native callsite
        InputStream is  = new FileInputStream(recordedNativeFile);
        BufferedReader buf = new BufferedReader(new InputStreamReader(is));
        String line = buf.readLine();
        while(line != null){
            nativeCallsite.add(line);
            line = buf.readLine();
        }
        is.close();
        buf.close();

        instrumentation.addTransformer(new Transformer(),true);
        //make sure the transformer get instrumented first
        for(Class<?> clazz : instrumentation.getAllLoadedClasses()) {
            if (instrumentation.isModifiableClass(clazz)) {
                instrumentation.retransformClasses(clazz);
            }
        }
    }

}
