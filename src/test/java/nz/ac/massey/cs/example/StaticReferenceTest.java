package nz.ac.massey.cs.example;

public class StaticReferenceTest {

    public static void main(String[] args){
            Bar.bar();
    }

    public static class Bar{
        public static void bar(){

        }
    }
}
