package nz.ac.massey.cs.example;

/**
 * main()->a();
 * a()->b();
 * a()->c();
 */
public class NestExceptionTest {

    public static void main(String[] args){
        a();
    }


    public static void b(){
        int arr[]= {1,2,3,4};
        int a=arr[10];
    }

    public static void a() {

        try{
            StringBuilder sb =new StringBuilder("s");
            try{
                b();
            }catch(Throwable e){
               c();
            }
        }
        catch(Throwable e){
            d();
        }
    }

    public static void c() {
    }
    //will not be invoked
    public static void d(){
    }
}
