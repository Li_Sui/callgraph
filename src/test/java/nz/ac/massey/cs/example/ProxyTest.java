package nz.ac.massey.cs.example;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

public class ProxyTest {
    public static void main(String[] args) throws Exception {
        ProxyTest dp =new ProxyTest();
        MyInterface proxy = (MyInterface) Proxy.newProxyInstance(MyInterface.class.getClassLoader(),
                new Class[] { MyInterface.class }, dp.new MyInvocationHandler());
        proxy.foo();
    }

    public interface MyInterface {
        void foo();
    }


    public class TestImple implements MyInterface{

        @Override
        public void foo() {
        }

    }
    public class MyInvocationHandler implements InvocationHandler {
        @Override
        public Object invoke(Object obj, Method m, Object[] arg)throws Throwable  {

            return  m.invoke(new TestImple (), arg);
        }
    }

}
