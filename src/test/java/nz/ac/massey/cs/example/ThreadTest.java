package nz.ac.massey.cs.example;

/**
 * edges that we are interested in:
 * main->Thread.start()?->ThreadTest$1.run()->a()
 * main->Thread.start()?->ThreadTest$2.run()->b()
 */
public class ThreadTest {

    public static void main(String[] args) {

        new Thread("t1") {
            public void run(){
                new ThreadTest().a();
            }
        }.start();

        Thread t2 = new Thread("t2") {
            public void run(){
                b();
            }
        };
        t2.start();
      ThreadFoo foo=new ThreadFoo(){

          @Override
          public void start() {

          }
      };
      //this is not a thread.start()
      foo.start();

    }

    public void a() {
    }
    public static void b() {
    }

    public interface ThreadFoo {
        public void start();
    }
}
