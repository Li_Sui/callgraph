package nz.ac.massey.cs.example;

import java.lang.invoke.MethodHandle;
import java.lang.invoke.MethodHandles;
import java.lang.invoke.MethodType;

public class MethodHandleTest {

    public static void main(String[] args) throws Throwable {
        new MethodHandleTest().foo();
    }

    public void foo() throws Throwable{
        MethodType methodType = MethodType.methodType(void.class);
        MethodHandle handle = MethodHandles.lookup().findVirtual(MethodHandleTest.class, "bar", methodType);
        handle.invoke(new MethodHandleTest());
    }

    public void bar(){

    }
}
