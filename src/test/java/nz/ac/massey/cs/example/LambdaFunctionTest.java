package nz.ac.massey.cs.example;

public class LambdaFunctionTest {

    public static void main(String[] args) throws Exception {
        LambdaFunctionTest test=new LambdaFunctionTest();
        java.util.function.Function<Integer, String> c = (i) -> test.target1();
        c.apply(3);

        java.util.function.Supplier<String> i = () ->test.target2("input");
        i.get();

        java.util.function.Consumer<String> k = (input) -> test.target3();
        k.accept("input");
    }

    public String target1() {
        return "";
    }

    public String target2(String input) {
        return "";
    }

    public void target3() {

    }
}
