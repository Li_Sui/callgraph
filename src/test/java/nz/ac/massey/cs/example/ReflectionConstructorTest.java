package nz.ac.massey.cs.example;

/**
 * use reflection to instantiate a class without providing string constant
 */

public class ReflectionConstructorTest {

    public ReflectionConstructorTest(){
    }
    public static void main(String[] args) throws Exception{
        new ReflectionConstructorTest().foo();
    }
    public void foo( ) throws Exception{
        ReflectionConstructorTest con=factory();
        con.bar();
    }

    public void bar(){

    }
    public ReflectionConstructorTest factory() throws Exception{
        return (ReflectionConstructorTest)Class.forName("nz.ac.massey.cs.example.ReflectionConstructorTest").getConstructor().newInstance();
    }
}
