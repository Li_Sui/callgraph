package nz.ac.massey.cs.example;

/**
 * main()-> FinalizerTest.<init>;
 *
 * finalize()->foo();
 */
public class FinalizerTest {

    public static void main(String[] args)throws Exception{
        FinalizerTest f= new FinalizerTest();

        f=null;

        System.gc();
    }

    public  void foo() throws Throwable{
        System.out.println("Finalizer");
    }

    @Override
    protected void finalize() throws Throwable {
        try {
            foo();
        } finally {
            super.finalize();
        }
    }
}
