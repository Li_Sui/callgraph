package nz.ac.massey.cs.example;

public class ShutDownHookTest {

    public static void main(String[] args)throws Exception{

        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                foo();
            }
        });
    }

    public static void foo(){
        System.out.println("foo "+Thread.currentThread().getName());
    }
}
