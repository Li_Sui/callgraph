package nz.ac.massey.cs.example;


/**
 * call graph:
 */
public class SimpleTest {

    public static void main(String[] args) throws Exception{

        SimpleTest test=new SimpleTest();
        test.A();
        test.B();

    }

    public void A(){
        C();
    }
    public void B(){
        C();
    }

    public void C(){
        bar();
        foo();
    }

    public void bar() {

    }

    public void foo() {

    }

}
