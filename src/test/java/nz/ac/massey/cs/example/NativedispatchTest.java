package nz.ac.massey.cs.example;

import java.io.File;


/**
 * main()-><NATIVE>dispatchCall()->target()
 *
 */
public class NativedispatchTest {

    static{
        System.load(new File("src/test/resources/nativedispatch.so").getAbsolutePath());

    }

    public static void main(String[] args){
        new NativedispatchTest().dispatchCall();

    }

    public native void dispatchCall();

    public void target(){}
}
