package nz.ac.massey.cs.example;

import java.util.ServiceLoader;

public class ServiceLoaderTest {

    public static void main(String[] args) {
        ServiceLoader<ServiceLoaderTest.ServiceIF> serviceLoader = ServiceLoader.load(ServiceLoaderTest.ServiceIF.class);

        for(ServiceIF s : serviceLoader){
            s.foo();
        }
    }

    public interface ServiceIF {
        public void foo();
    }

}
