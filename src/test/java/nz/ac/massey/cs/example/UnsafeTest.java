package nz.ac.massey.cs.example;

import java.lang.reflect.Field;

public class UnsafeTest {
    SimpleTest value= new SimpleTest();
    public static void main(String[] args) throws Exception{
        long objectOffset = getUnsafe().objectFieldOffset(UnsafeTest.class.getDeclaredField("value"));
        Object value = getUnsafe().getObject(new UnsafeTest(), objectOffset);

        ((SimpleTest)value).foo();
    }

    public static sun.misc.Unsafe getUnsafe() {
        sun.misc.Unsafe unsafe = null;
        try {
            Field f = sun.misc.Unsafe.class.getDeclaredField("theUnsafe");
            f.setAccessible(true);

            unsafe = (sun.misc.Unsafe) f.get(null);
        } catch (Exception e) {
        }
        return unsafe;
    }
}
