package nz.ac.massey.cs.example;

/**
 * call graph:
 * main->a()->b()->c();
 * main->a()->d();
 * main->a()->e();
 */
public class ExceptionTest {
    public static void main(String[] args){
        a();
    }


    public static void c() {
            int[] array={1};
            int i=array[1];
    }

    public static void b(){

            c();
    }

    public static void a() {
        try {

            b();

        }catch(Exception e){
            d();
        }finally{
            e();
        }
    }



    public static void d(){
    }


    public static void e(){
    }


}
