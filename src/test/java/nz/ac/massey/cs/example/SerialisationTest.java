package nz.ac.massey.cs.example;
import java.io.*;

public class SerialisationTest implements Serializable{
    public static void main(String[] args) throws Exception{
        new SerialisationTest().foo();
        //code used to serialise an object
        //serialise(new SerialisationTest().new Bar());
    }

    public void foo( ) throws Exception{
        BarInterface obj=factory();
        obj.bar();
    }

    public BarInterface factory() throws Exception{
        FileInputStream fileIn = new FileInputStream("src/test/resources/object.ser");
        ObjectInputStream ois = new ObjectInputStream(fileIn);
        BarInterface bar =(BarInterface)ois.readObject();
        ois.close();
        fileIn.close();
        return bar;
    }
    //code used to serialise an object
    public static void serialise(Object obj) throws Exception {
        FileOutputStream fileOut =
                new FileOutputStream("src/test/resources/object.ser");
        ByteArrayOutputStream ba = new ByteArrayOutputStream();
        ObjectOutputStream oos = new ObjectOutputStream(fileOut);
        oos.writeObject(obj);
        oos.close();
        fileOut.close();

    }

    public interface BarInterface {
        void bar();
    }

    public class Bar implements BarInterface, Serializable {
        private static final long serialVersionUID = 1L;
        @Override
        public void bar(){}
    }
}
