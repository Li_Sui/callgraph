package nz.ac.massey.cs.example;

import java.util.Arrays;
import java.util.List;

public class InvokeDynamicTest {

    public static void main(String[] args) {
        List<String> list= Arrays.asList("a");

        list.stream().forEach(element->{new InvokeDynamicTest().foo();});
    }

    public void foo(){

    }
}
