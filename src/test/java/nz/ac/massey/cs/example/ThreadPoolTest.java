package nz.ac.massey.cs.example;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * edges that we are interested in:
 * main->ExecutorService.execute()?->WorkerThread1.run()->a()
 * main->ExecutorService.execute()?->WorkerThread2.run()->b()
 * main->ExecutorService.execute()?->WorkerThread3.run()->c()
 */
public class ThreadPoolTest {
    public static void main(String[] args) {
        ExecutorService executor = Executors.newFixedThreadPool(2);
        foo1(executor);
        bar(executor);
        foo2(executor);
        executor.shutdown();
        while (!executor.isTerminated()) {
        }
    }

    public static void foo1(ExecutorService executor){
        executor.execute(new ThreadPoolTest().new WorkerThread1());
    }

    public static void bar(ExecutorService executor){
        executor.execute(new ThreadPoolTest().new WorkerThread2());
    }

    public static void foo2(ExecutorService executor){
        executor.execute(new ThreadPoolTest().new WorkerThread3());
    }

    public class WorkerThread1 implements Runnable {
        @Override
        public void run() {
            a();
        }
    }

    public class WorkerThread2 implements Runnable {
        @Override
        public void run() {
            b();
        }
    }

    public class WorkerThread3 implements Runnable {
        @Override
        public void run() {
            c();
        }
    }


    static void a(){
        //System.out.println("a:"+ Thread.currentThread().getId()+" hashcode:"+System.identityHashCode(Thread.currentThread()));
    }
    static void b(){
        //System.out.println("b:"+ Thread.currentThread().getId()+" hashcode:"+System.identityHashCode(Thread.currentThread()));
    }

    static void c(){
        //System.out.println("c:"+ Thread.currentThread().getId()+" hashcode:"+System.identityHashCode(Thread.currentThread()));
    }
}
