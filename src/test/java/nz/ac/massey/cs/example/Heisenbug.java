package nz.ac.massey.cs.example;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * We found this Heisenbug where running instrumented code may return target2()
 * Normal execution return targe1()
 */
public class Heisenbug {
    public static void main(String[] args) throws Exception{
        for (java.lang.reflect.Method method : Heisenbug.class.getDeclaredMethods()) {
            if (method.isAnnotationPresent(Method.class)) {
                method.invoke(new Heisenbug(), null);
                return;
            }
        }
    }


    @Method
    public void target1() {
        System.out.println("target1");
    }

    @Method
    public void target2() {
        System.out.println("target2");
    }

    @Retention(RetentionPolicy.RUNTIME)
    @java.lang.annotation.Target(ElementType.METHOD)
    public @interface Method {
    }
}