package nz.ac.massey.cs.example;
import javax.swing.*;
import java.awt.*;

public class AsynchronousCallback extends JPanel {

    public static void main ( String [] args ) throws Exception
    {
        JFrame window = new JFrame();
        window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        window.setBounds(30, 30, 600, 600);
        window.getContentPane().add(new AsynchronousCallback());
        window.setVisible(true);

    }
    @Override
    public void paint(Graphics g) {
        Image img =Toolkit.getDefaultToolkit().getImage("image.jpg");
        g.drawImage(img,0,0,this);
    }

    @Override
    public boolean imageUpdate(Image img1,int flags, int xAxis, int yAxis, int width, int height)
    {   //if finish it repaint it
        if ((flags & ALLBITS) != 0){

            repaint(xAxis,yAxis,width,height);
        }
        //else continue to paint
        return (flags & ALLBITS) == 0;
    }
}