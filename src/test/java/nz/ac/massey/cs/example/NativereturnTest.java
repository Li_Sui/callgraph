package nz.ac.massey.cs.example;

import java.io.File;

/**
 * main()-><NATIVE>getObject()->target()
 * returned object should be tags
 *
 */
public class NativereturnTest {

    static{
        System.load(new File("src/test/resources/nativereturn.so").getAbsolutePath());

    }

    public static void main(String[] args){
        NativereturnTest obj= (NativereturnTest)new NativereturnTest().getObject();
        obj.target();
    }

    public native Object getObject();

    public void target(){}


}
