package nz.ac.massey.cs.example;


public class DynamicAllocationTest{

    public static void main(String[] args) throws Exception{
        new DynamicAllocationTest().foo();
    }

    public void foo( ) throws Exception{
        DynamicAllocationTest obj=factory();
        obj.bar();
    }

    public DynamicAllocationTest factory() throws Exception{
        return (DynamicAllocationTest)Class.forName("nz.ac.massey.cs.example.DynamicAllocationTest").newInstance();
    }
    public void bar(){
    }
}
