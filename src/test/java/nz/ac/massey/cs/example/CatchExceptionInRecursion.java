package nz.ac.massey.cs.example;


public class CatchExceptionInRecursion {

    static int count=0;
    public static void main(String[] args){

        recursion();
        foo();

    }
    static void recursion() {

        count++;
        if(count<=3) {
            try {
                recursion();
            } catch (Exception e) {}
        }

        if (count==4){
            count++;
            int[] array={1};
            int i=array[1];
        }

    }

    static void foo(){

    }
}
