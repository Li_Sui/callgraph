package nz.ac.massey.cs.example;

import java.lang.reflect.Field;

public class ReflectionFieldTest {

    DynamicAllocationTest f =new DynamicAllocationTest();

    public static void main(String[] args) throws Exception{
        new ReflectionFieldTest().foo();
    }

    public void foo( ) throws Exception{
        DynamicAllocationTest obj=factory();
        obj.bar();
    }

    public DynamicAllocationTest factory()throws Exception{
        Field field=  ReflectionFieldTest.class.getDeclaredField("f");
        ReflectionFieldTest instance =new ReflectionFieldTest();
        return (DynamicAllocationTest)field.get(instance);
    }
}
