package nz.ac.massey.cs.example;

import java.io.File;
import java.net.URL;
import java.net.URLClassLoader;

/**
 * In multi-classloader environments the plain name of a class does not unambiguously identify a class.
 * For example OSGi allows to use different versions of the same class to be loaded within the same VM
 */
public class ClassloaderTest  extends ClassLoader {

    public static void main(String[] args) throws Exception{
        method1();
        method2();
    }

    public static void method1() throws Exception{
       // System.out.println( nz.ac.massey.cs.example.ClassloaderTest.class.getClassLoader()+"dddd");
        URLClassLoader cl=new URLClassLoader(new URL[]{new File("src/test/resources/v1.jar").toURL()}, Thread.currentThread().getContextClassLoader());
        cl.loadClass("nz.ac.massey.cs.test.Test").newInstance();

    }
    public static void method2() throws Exception{
        //System.out.println( nz.ac.massey.cs.example.ClassloaderTest.class.getClassLoader()+"dddd");
        URLClassLoader cl=new URLClassLoader(new URL[]{new File("src/test/resources/v2.jar").toURL()}, Thread.currentThread().getContextClassLoader());
        cl.loadClass("nz.ac.massey.cs.test.Test").newInstance();
    }

}
