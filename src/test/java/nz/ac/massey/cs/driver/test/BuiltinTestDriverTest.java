package nz.ac.massey.cs.driver.test;

import javassist.*;
import javassist.bytecode.AnnotationsAttribute;
import javassist.bytecode.annotation.Annotation;
import nz.ac.massey.cs.driver.BuiltinTestDriverGenerator;
import nz.ac.massey.cs.driver.JUnitModel;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class BuiltinTestDriverTest {

    String beforeClassSource="public void beforeClass(){}";
    String beforeSource="public void setUp(){}";
    String testSource="public void test(){}";
    String afterSource="public void tearDown(){}";
    String afterClassSource="public void afterClass(){}";

    ClassPool pool ;
    @Before
    public void setUp(){
        pool = ClassPool.getDefault();
    }
    @After
    public void tearDown(){
        pool=null;
    }

    /**
     * source representation: basic junit4 model
     * -----------------------------
     * public class Foo{
     *
     * @BeforeClass
     * public void beforeClass(){}
     *
     * @Before
     * public void setUp(){}
     *
     * @Test
     * public void test(){}
     *
     * @After
     * public void tearDown(){}
     * @AfterClass
     * public void afterClass(){}
     *
     * }
     *-----------------------------
     * convert to:
     * public class Driver0{
     *  public static void main(String[] args) throws Throwable{
     *      try{
     *       Foo driver =new Foo();
     *       driver.beforeClass();
     *        try{
     *          driver.setUp();
     *          driver.test();
     *          driver.tearDown();
     *        }catch(Throwable e){System.err.println(e.getMessage());}
     *        driver.afterClass();
     *      }catch(Throwable e){System.err.println(e.getMessage());}
 *      }
     *}
     *-----------------------------
     *
     * @throws Exception
     */
    @Test
    public void testBasicJunit4Model() throws Exception{

        CtClass testClass=pool.makeClass("Foo");
        CtMethod beforeClassMethod=CtNewMethod.make(beforeClassSource,testClass);
        beforeClassMethod.getMethodInfo().addAttribute(genMethodAttribute(beforeClassMethod,org.junit.BeforeClass.class.getName()));
        testClass.addMethod(beforeClassMethod);

        CtMethod beforeMethod=CtNewMethod.make(beforeSource,testClass);
        beforeMethod.getMethodInfo().addAttribute(genMethodAttribute(beforeMethod,org.junit.Before.class.getName()));
        testClass.addMethod(beforeMethod);

        CtMethod testMethod=CtNewMethod.make(testSource,testClass);
        testMethod.getMethodInfo().addAttribute(genMethodAttribute(testMethod,org.junit.Test.class.getName()));
        testClass.addMethod(testMethod);

        CtMethod afterMethod=CtNewMethod.make(afterSource,testClass);
        afterMethod.getMethodInfo().addAttribute(genMethodAttribute(afterMethod,org.junit.After.class.getName()));
        testClass.addMethod(afterMethod);

        CtMethod afterClassMethod=CtNewMethod.make(afterClassSource,testClass);
        afterClassMethod.getMethodInfo().addAttribute(genMethodAttribute(afterClassMethod,org.junit.AfterClass.class.getName()));
        testClass.addMethod(afterClassMethod);


        JUnitModel model=new JUnitModel();

        BuiltinTestDriverGenerator.anaylseMethodAnnotation(testClass,model);

        Assert.assertEquals("beforeClass",model.getBeforeClass());
        Assert.assertEquals("setUp",model.getBefore());
        Assert.assertEquals("test",model.getTestMethods().get(0));
        Assert.assertEquals("tearDown",model.getAfter());
        Assert.assertEquals("afterClass",model.getAfterClass());
    }

    /**
     * source representation: Parameterized tests in Junit4
     * -----------------------------
     * public class Foo{
     *
     * public int a;
     * public Foo b;
     *
     * public Foo(int a, Foo b){
     *     this.a=a;
     *     this.b=b;
     * }
     * @Test
     * public void test(){}
     * }
     * @Parameters
     * public static Collection data(){}
     *}
     *
     * -----------------------------
     * convert to :
     * -----------------------------
     * public class Driver0{
     *  public static void main(String[] args) throws Throwable{
     *      try{
     *          for(Object[] data:Foo.data()){
     *              Foo driver= new Foo((int)data[0],(Foo)data[1]);
     *              try{
     *                  driver.test();
     *              }catch(Throwable e){System.err.println(e.getMessage());}
     *          }
     *      }catch(Throwable e){System.err.println(e.getMessage());}
     *  }
     * }
     *
     *
     * @throws Exception
     */
    @Test
    public void testParameterizedJunit4Model() throws Exception{

        CtClass testClass=pool.makeClass("Foo");

        CtMethod testMethod=CtNewMethod.make(testSource,testClass);
        testMethod.getMethodInfo().addAttribute(genMethodAttribute(testMethod,org.junit.Test.class.getName()));
        testClass.addMethod(testMethod);

        CtMethod parameterMethod=CtNewMethod.make("public static void data(){}",testClass);
        parameterMethod.getMethodInfo().addAttribute(genMethodAttribute(parameterMethod,org.junit.runners.Parameterized.Parameters.class.getName()));
        testClass.addMethod(parameterMethod);

        testClass.addField(CtField.make("public int a;",testClass));
        testClass.addField(CtField.make("public Foo b;",testClass));
        testClass.addConstructor(CtNewConstructor.make("public Foo(int a, Foo b){this.a=a;this.b=b;}",testClass));

        JUnitModel model=new JUnitModel();

        BuiltinTestDriverGenerator.anaylseMethodAnnotation(testClass,model);

        Assert.assertEquals("data",model.getParameterizedMethod());
        Assert.assertEquals("(int)",model.getConstructorParam().get(0));
        Assert.assertEquals("(Foo)",model.getConstructorParam().get(1));
        Assert.assertEquals("test",model.getTestMethods().get(0));
    }
    /**
     * source representation:Basic Junit3 Model
     * -----------------------------
     * public class Foo extend TestCases{
     *
     * public void setUp(){}
     *
     * public void test(){}
     *
     * public void tearDown(){}
     *
     * }
     *-----------------------------
     * convert to:
     * public class Driver0{
     *  public static void main(String[] args) throws Throwable{
     *      try{
     *       Foo driver =new Foo();
     *        try{
     *          driver.setUp();
     *          driver.test();
     *          driver.tearDown();
     *        }catch(Throwable e){System.err.println(e.getMessage());}
     *      }catch(Throwable e){System.err.println(e.getMessage());}
     *      }
     *}
     *-----------------------------
     *
     * @throws Exception
     */
    @Test
    public void testBasicJunit3Model() throws Exception{
        CtClass testClass=pool.makeClass("Foo");

        CtMethod beforeMethod=CtNewMethod.make(beforeSource,testClass);
        testClass.addMethod(beforeMethod);

        CtMethod testMethod=CtNewMethod.make(testSource,testClass);
        testClass.addMethod(testMethod);

        CtMethod afterMethod=CtNewMethod.make(afterSource,testClass);
        testClass.addMethod(afterMethod);

        JUnitModel model=new JUnitModel();

        BuiltinTestDriverGenerator.anaylseJUnit3Method(testClass,model);


        Assert.assertEquals("setUp",model.getBefore());
        Assert.assertEquals("test",model.getTestMethods().get(0));
        Assert.assertEquals("tearDown",model.getAfter());
    }

    private AnnotationsAttribute genMethodAttribute(CtMethod method,String annotation) {
        Annotation annotationNew = new Annotation(annotation,  method.getMethodInfo().getConstPool());
        AnnotationsAttribute attributeNew = new AnnotationsAttribute(method.getMethodInfo().getConstPool(), AnnotationsAttribute.visibleTag);
        attributeNew.setAnnotation(annotationNew);
        return attributeNew;
    }
}
