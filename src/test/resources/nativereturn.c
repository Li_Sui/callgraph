#include <stdio.h>
#include <jni.h>
#include "nativereturn.h"

JNIEXPORT jobject JNICALL
Java_nz_ac_massey_cs_example_NativereturnTest_getObject(JNIEnv *env, jobject obj)
{
  jclass class = (*env)->FindClass(env,"nz/ac/massey/cs/example/NativereturnTest");
  jmethodID mid = (*env)->GetMethodID(env, class, "<init>", "()V");

  return (*env)->NewObject(env, class, mid);
}
