#include <stdio.h>
#include <jni.h>
#include "nativedispatch.h"

JNIEXPORT void JNICALL
Java_nz_ac_massey_cs_example_NativedispatchTest_dispatchCall(JNIEnv *env, jobject obj)
{
  jclass class = (*env)->GetObjectClass(env, obj);
  jmethodID mid = (*env)->GetMethodID(env, class, "target", "()V");
  (*env)->CallVoidMethod(env, obj, mid);
}
